tupleFromInputString :: String -> Maybe (String, String, Int)
tupleFromInputString str = if len == 3 then  Just (first,second,age) else Nothing
  where len = length strs
        strs = words str 
        first = head strs
        second = strs !! 1
        age = read (strs !! 2) :: Int

data Person = Person {
  firstName :: String
  ,secondName :: String
  ,age :: Int
  } 

personFromTuple ::  (String,String,Int) -> Person
personFromTuple (fName,sName,age) = Person fName sName age

convertTuple :: Maybe (String,String,Int) -> Maybe Person
convertTuple Nothing = Nothing
convertTuple (Just (fName,sName,age)) = Just ( Person  fName sName  age)



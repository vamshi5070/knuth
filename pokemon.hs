import System.Random

main = gameLoop 40 30

gameLoop :: Int -> Int -> IO ()
gameLoop hp1 hp2 = do
  putStrLn ""
  putStr $ "You: charlemon: "  <> "hp: "
  print hp2
  putStr $ "Rival: raichu: " <>   "hp: "
  print hp1
  putStr "Attack raichu with: "
  attack <- getInt
  resHp1 <- rivalDamage attack hp1
  resHp2 <- userDamage hp2
  if resHp1 <= 0 then
    putStrLn "Games ends!! You are the winner."
    else
         if resHp2 <= 0 then
           putStrLn "Games ends!! You lost the game"
         else
           gameLoop resHp1 resHp2

getInt :: IO Int
getInt =  read <$> getLine

rivalDamage :: Int -> Int -> IO Int
rivalDamage 1 hp1 = do
                reduction <- randomRIO (3, 7)   
                return $ hp1 - reduction 
rivalDamage 2 hp1 = do
                reduction <- randomRIO (7, 11)  
                return $ hp1 - reduction 

userDamage :: Int -> IO Int
userDamage hp2 = do
                reduction <- randomRIO (1, 10)    
                return $ hp2 - reduction 

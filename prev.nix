{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        haskell-env = pkgs.haskellPackages.ghcWithHoogle (hp:
          with hp; [
            brick
            cursor
            cursor-brick
            directory
            path
            path-io
            pretty-show
            text
            vty
            aeson
            http-conduit
            yesod
          ]);
        miscPkgs = with pkgs; [
          cabal-install
          ghcid
          # haskell-language-server
        ];
      in {
        devShell = pkgs.mkShell { buildInputs = [ haskell-env ] ++ miscPkgs; };
      });
}

import Data.Map as M

myList = M.fromList [(1,"monad"),(3,"applicative"),(5,"functor")]

myList' = M.fromList [(1,"monad"),(3,"applicative"),(3,"monoid")]

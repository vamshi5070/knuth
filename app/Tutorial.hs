main = do
  putStrLn "haskell"

id' :: a -> a
id' x = x

int1To5 :: [Int]
int1To5 = [1,2,3,4,5]

add :: Int -> Int -> Int
add x y = x + y

add' = (\x y -> x + y)

inc' :: Int -> Int
inc' num = num + 1

a = 2

repeat' :: a -> [a]
repeat' ele  = ele:repeat' ele 

replicate' :: Int -> a -> [a]
replicate' 0 ele = []
replicate' n ele = ele:replicate' (n-1) ele

length' :: [a] -> Int
length' [] = 0
length' (_:xs) = 1 + length' xs

add'' :: Int -> Int -> Int
add'' 0 y = y
add'' x y = inc' (add'' (x-1) y)

elem' :: (Eq a) => a -> [a] -> Bool
elem' _   [] = False
elem' ele (x:xs)    
  | ele == x = True
  | otherwise = elem' ele xs

multi :: Int -> Int -> Int
multi 1 y = y
multi x y = add'' y (multi (x-1) y)

fizzBuzz :: Int -> String 
fizzBuzz num
  | mod num 3 == 0 = "fizz"
  | mod num 5 == 0 = "buzz"
  | otherwise = "fizzBuzz"

head' :: [a] -> a
head' [] = error "empty list!!"
head' (x:_) = x

tail' :: [a] -> [a]
tail' [] = []
tail' (_:xs) = xs

init' :: [a] -> [a]
init' [] = []
init' [x] = []
init' (x:xs) = x:init' xs

last' :: [a] -> a
last' [] = error "empty list!!"
last' [x] = x
last' (_:xs) = last' xs

access :: [a] -> Int -> a  -- similar to (!!)
access [] _ = error "index out of range"
access (x:_) 0 = x
access (x:xs) n 
  | n > 0 = access xs (n-1)
  | otherwise = error "index out of range"

fst' :: (a, b) -> a
fst' (x,_) = x

snd' :: (a, b) -> b
snd' (_,y) = y

curry' :: ((a, b) -> c) -> a -> b -> c
curry' f x y = f (x,y)

uncurry' :: (a -> b -> c) -> (a, b) -> c
uncurry' f (x,y) = f x y

max' :: Int -> Int -> Int
max' x y
  | x <= y = y
  | otherwise = x

min' :: Int -> Int -> Int
min' x y
  | x <= y = x
  | otherwise = y

minimum' :: [Int] -> Int
minimum' [] = error "empty list"
minimum' [x] = x
minimum' (x:xs) = min' x (minimum' xs)

maximum' :: [Int] -> Int
maximum' [] = error "empty list"
maximum' [x] = x
maximum' (x:xs) = max' x (maximum' xs)

delete :: (Eq a) => a -> [a]  -> [a]
delete _ [] = error "element not found"
delete ele (x:xs)
  | ele == x = xs
  | otherwise = x : delete ele xs


-- div' :: Integral a => a -> a -> a
-- div' x y
  -- | x == y = 1
  -- | x < 

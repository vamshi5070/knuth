import Data.Foldable
import Data.Monoid

boop = (*2)

doop = (+10)

bbop :: Integer -> Integer
bbop = (+) <$> boop <*> doop

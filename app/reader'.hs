newtype Reader' r a =
  Reader' { runReader' :: r -> a }

instance Functor (Reader' r ) where
  fmap f ra = Reader' $ f . g
    where g = runReader' ra

instance Applicative (Reader' r ) where
   pure a = Reader' $ (\x y -> x) a
   (<*>) fa ra = Reader' $ (\r -> (fa' r) (ra' r) )
     where fa' = runReader' fa
           ra' = runReader' ra

instance Monad (Reader' r ) where
   return = pure
   m >>= k = Reader' $ (\r -> runReader' (k (runReader' m r)) r )

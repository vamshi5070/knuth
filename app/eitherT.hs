newtype EitherT  e m a =
  EitherT { runEitherT :: m (Either e a)}

instance (Functor f) => Functor (EitherT  e f)  where
  fmap f (EitherT a) = EitherT $ (fmap . fmap)  f a

instance (Applicative f) => Applicative (EitherT  e f)  where
  pure a = EitherT $ (pure . pure) a
  (<*>) (EitherT f) (EitherT a) = EitherT $ (<*>) <$> f <*> a

instance Monad m => Monad (EitherT  e m) where
  return  = pure
  (>>=) ma f = EitherT $ do
    val <- runEitherT ma
    case val of
      Left n -> return $ Left n
      Right n -> runEitherT $ f n

swapEither :: Either e a -> Either a e
swapEither (Left n) = Right n
swapEither (Right n) = Left n

-- swapEitherT :: (Functor m) => EitherT e m a -> EitherT a m e
-- swapEitherT val = swapEither <$> val

eitherT :: (Monad m) => (a -> m c) -> (b -> m c) -> EitherT a m b -> m c
eitherT f g rgv = do
  rgv' <- runEitherT rgv
  case  rgv' of
    Left x ->  f x
    (Right x) -> g x

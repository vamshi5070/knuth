countGreen :: String -> Int
countGreen  str = length [x | x <- str,x == 'G' ]

countRed :: String -> Int
countRed  str = length [x | x <- str,x == 'R' ]

countYellow :: String -> Int
countYellow  str = length [x | x <- str,x == 'Y' ]

countBlue :: String -> Int
countBlue  str = length [x | x <- str,x == 'B' ]

-- generatePrefix [] = []
-- generatePrefix xs = foldl (\acc x -> (x:(head acc)):acc  ) [[]] xs

generatePrefix xs = helper xs []
  where helper xs ys
          | length xs > 1 = helper (take ((length xs)-1) xs) (take ((length xs)-1) xs:ys)
          | otherwise = ys

    --helper [] ys = ys
-- xmonad xs = 

sapno xs = (countGreen xs == countRed xs) && (countBlue xs == countYellow xs)

rod xs = and map countGreen  $ generatePrefix xs

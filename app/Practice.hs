even' :: Int -> Bool
even' 0 = True
even' n = odd' $ n-1

odd' ::  Int -> Bool
odd' 0 = False
odd' n  = even' $ n-1

evens :: [a] -> [a]
evens [] = []
evens (x:xs) = x:odds xs

odds :: [a] -> [a]
odds [] = []
odds (_:xs) = evens xs

product' :: (Num a) => [a] -> a
product' = foldr (*) 1
-- product' [] = 1
-- product' (x:xs) = x*product' xs 

digits :: Int -> [Int]
digits  = reverse . helper 
  where
    helper num
      | num < 10 = [num]
      | otherwise = mod num 10 : helper (div num 10)

type Bit = Int

bin2int :: [Bit] -> Int
bin2int  = snd . foldr (\x (bit,res) -> (bit*2,res+bit*x) ) (1,0) 

int2bin :: Int -> [Bit]
int2bin 0 = []
int2bin num = (mod num 2) : (int2bin $ div num 2)

make8 :: [Bit] -> [Bit]
make8 xs = take 8 (xs ++ repeat 0)

dec2Int :: [Int] -> Int
dec2Int  = snd . foldr (\x (bit,acc)  -> (bit*10,bit*x+acc)) (1,0) 

curry' :: ((a,b) -> c) -> a -> b -> c
curry' f a b = f (a,b)

uncurry' :: (a -> b -> c) -> (a, b) -> c
uncurry' f (x,y) = f x y

unfold :: (a -> Bool) -> (a -> b) -> (a -> a) -> a -> [b]
unfold p h t x
  | p x = []
  | otherwise = h x : unfold p h t (t x)

map' :: Eq a => (a -> b) -> [a] -> [b]
map' f xs = unfold (==[]) (f . head) tail xs

iterate' :: (a -> a) -> a -> [a]
-- iterate' f x = f x : iterate' f (f x)
iterate' f  = unfold (\_ -> False) f f 

chop8 :: Eq a => [a] -> [[a]]
chop8 = unfold (==[]) (take 8) (drop 8)

encode :: String -> [Bit]
encode = concatMap (make8 . int2bin . ord)

decode ::[Bit] ->  String 
decode = map (chr . bin2int) . chop8

transmit :: String -> String
transmit = decode . channel . encode

channel :: [Bit] -> [Bit]
channel = id

data List a = Nil
            | Cons a (List a) 
            deriving (Eq,Show)

instance Semigroup (List a) where
  (<>) Nil Nil = Nil
  (<>) Nil xs = xs
  (<>)  xs Nil = xs
  (<>) (Cons a xs) ys  = Cons a ( xs <> ys)

instance Monoid (List a) where
  mempty = Nil
  mappend = (<>)

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons a xs) = Cons  (f a) $ fmap f xs

instance Applicative List where
  pure a = Cons a Nil
  (<*>) _ Nil = Nil
  (<*>) Nil _ = Nil
  (<*>) (Cons f fs) (Cons x xs) = Cons (f x) ((<*>) fs xs)

instance Monad List where
  return = pure
  (>>=) xs f = concat' $ fmap f xs

concat' :: List (List a) -> List a
concat'  Nil = Nil
concat' (Cons x xs) = x `mappend` concat' xs


newtype Writer' w a =
  Writer' { runWriter' ::  (a,w) }

instance Functor (Writer' w) where
  fmap f wa = Writer' (f x , w)
    where (x,w) = runWriter' wa

instance (Monoid w) => Applicative (Writer' w) where
  pure x = Writer' (x,mempty)
  (<*>) fa wa = Writer' (fx wx,fw <> ww)
    where (fx,fw) = runWriter' fa
          (wx,ww) = runWriter' wa

instance (Monoid w) => Monad (Writer' w) where
  return = pure
  m >>= k = Writer' (x' , w <> w')
    where (x,w) = runWriter' m
          (x',w') = runWriter' $ k x

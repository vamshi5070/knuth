import Data.Char
import Control.Applicative
import Data.Maybe

main :: IO ()
main = do
  print $
    sequenceA [Just 3,Just 2,Just 1]
  print $ sequenceA [x,y]
  print $ sequenceA [xs, ys]
  print $ summed <$> ((,) <$> xs <*> ys)
  print $ fmap summed ((,) <$> xs <*> zs)
  print $ bolt 7
  print $ fmap bolt z

s' = summed <$> ((,) <$> xs <*> ys)

sequA :: Integral a => a -> [Bool]
sequA m = sequenceA [(>3), (<8), even] m

cap xs = map toUpper xs

rev :: [Char] -> [Char]
rev xs = reverse xs

composed :: String -> (String,String)
composed str = (str , cap $ rev str)

boop = (*2)
doop = (+10)

fmapped :: String -> String
fmapped  = cap <$> rev 

tupled :: [Char] -> ([Char],[Char])
tupled str = (str,fmapped str) 

-- tupledMonad :: String -> String
tupledMonad str = do
  ch <- reverse str
  [toUpper ch]

tupledMonad' str =   reverse str >>= \ch -> [toUpper ch]
  -- [toUpper ch]

rgv str = do
  s <- str
  (+2) s:[]
  
boopDoop :: Integer -> Integer
boopDoop = do
  a <- boop
  b <- doop
  return (a + b)

newtype Reader' r a =
  Reader' { runReader' :: r -> a }
  -- deriving (Eq,Show)

instance Functor (Reader' r) where
  fmap f (Reader' ra) = Reader' (\x -> f (ra x))

instance Applicative (Reader' r) where
  pure a = Reader' $ (const a)
  (<*>) (Reader' ra) (Reader' rb) = Reader' $ \r -> ((ra r) (rb r))

instance Monad (Reader' r) where
  return = pure
  (>>=) ra aRb = (Reader' (\r -> runReader'( aRb (runReader' ra r )) r))  --aRb (runReader' ra)--Reader' $ \r -> ra r
    -- where Reader' $ \r -> ka = Reader' $ \r -> ra r  

data Person = Person {
  humanName :: HumanName
  , dogName :: DogName
  , address :: Address
  } deriving (Eq, Show)

data Dog = Dog {
  dogsName :: DogName
  , dogsAddress :: Address
  } deriving (Eq, Show)

newtype HumanName =
  HumanName String
  deriving (Eq, Show)

newtype DogName =
  DogName String
  deriving (Eq, Show)

newtype Address =
  Address String
  deriving (Eq, Show)

foldl' f acc [] = acc
foldl' f acc (x:xs) =  x `f` foldl' f acc xs

myLiftA2 :: Applicative f => (a -> b -> c) -> f a -> f b -> f c
myLiftA2 f x y = f <$> x <*> y  

x = [1, 2, 3]
y = [4, 5, 6]
z = [7, 8, 9]

xs = lookup 3 $ zip x y

ys = lookup 6 $ zip y z

-- zip x and y using 4 as the lookup key
zs :: Maybe Integer
zs = lookup 4 $ zip x y

z' :: Integer -> Maybe Integer
z' n = lookup n $ zip x z

x1 = (,) <$> xs <*> ys

x2 = (,) <$> ys <*> zs

x3 :: Integer -> (Maybe Integer, Maybe Integer)
x3 n = ((z' n), (z' n))

summed :: Num c => (c,c) -> c
summed = uncurry (+)

bolt :: Integer -> Bool
bolt = (&&) <$> (>3) <*> (<8)



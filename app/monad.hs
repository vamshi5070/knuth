import Control.Monad

class Container c where
  empty :: c a
  insert :: a -> c a -> c a

instance Container [] where
  empty = []
  insert  = (:)

newtype Queue a = Queue {unQueue :: [a]}

instance Container Queue where
  empty = Queue []
  insert ele xs  =  Queue $ (unQueue xs) ++ [ele]


data Tree a = Leaf a
              | Node (Tree a) (Tree a)
              deriving (Eq,Show)

import System.Random
import Control.Monad.State
import Control.Applicative

boop = (*2)
doop = (+10)

boopDoop :: Integer -> Integer
boopDoop = do
  a <- boop
  b <- doop
  return (a+b)

bbop :: Integer -> Integer
bbop = (+) <$> boop <*> doop

intToDie :: Int -> Die
intToDie n =
  case n of
    1 -> DieOne
    2 -> DieTwo
    3 -> DieThree
    4 -> DieFour
    5 -> DieFive
    6 -> DieSix
-- Use 'error'
-- _extremely_ sparingly.
    x ->
      error $
      "intToDie got non 1-6 integer: "
      ++ show x

-- Six-sided die
data Die =
  DieOne
  | DieTwo
  | DieThree
  | DieFour
  | DieFive
  | DieSix
  deriving (Eq, Show)

rollDieThreeTimes :: (Die, Die, Die)
rollDieThreeTimes = do
  let s = mkStdGen 12
      (d1, s1) = randomR (1, 6) s
      (d2, s2) = randomR (1, 6) s1
      (d3, _) = randomR (1, 6) s2
  (intToDie d1, intToDie d2, intToDie d3)

rollDie :: State StdGen Die
rollDie = state $ do
  (n, s) <- randomR (1, 6)
  return (intToDie n, s)

rollDieThreeTimes'
  :: State StdGen (Die, Die, Die)
rollDieThreeTimes' =
  liftA3 (,,) rollDie rollDie rollDie

infiniteDie :: State StdGen [Die]
infiniteDie = repeat <$> rollDie

nDie :: Int -> State StdGen [Die]
nDie n = replicateM n rollDie

rollsToGet20 :: StdGen -> Int
rollsToGet20  = go 0 0
  where
    go :: Int -> Int -> StdGen -> Int
    go sum count gen
      | sum >= 20 = count
      | otherwise = go (sum+die) (count+1) nextGen
        where (die,nextGen) = randomR (1,6) gen

rollsToGetN :: Int -> StdGen -> Int
rollsToGetN n  = go 0 0 
  where
    go ::  Int -> Int -> StdGen -> Int
    go count sum gen
      | sum >= n = count
      | otherwise = go  (count+1) (sum+die) nextGen
        where (die,nextGen) = randomR (1,6) gen

rollsCountLogged :: Int
  -> StdGen
  -> (Int, [Die])
rollsCountLogged n = go 0 0 []
  where
    go :: Int -> Int -> [Die] -> StdGen -> (Int,[Die])
    go count sum acc gen
      | sum >= n = (count,acc)
      | otherwise = go (count+1) (sum+die) (intToDie die:acc)  nextGen
        where (die,nextGen) = randomR (1,6) gen

newtype Moi s a =
  Moi { runMoi :: s -> (a, s) }

instance Functor (Moi s) where
  fmap f (Moi g) = Moi (\s -> let (a, s1) = g s
                               in (f a, s1))

instance Applicative (Moi s) where
  pure a = Moi (\s -> (a,s))
  (Moi f) <*> (Moi g) = Moi $ \s -> let (h,s') = g s
                                        (a,s'') = (f s')
                                    in (a h,s'')
                                  
instance Monad (Moi s) where
  return = pure
  (Moi f) >>= g = Moi $ \s -> let (a,s') = f s
                               in runMoi (g a) s'

getMoi :: Moi s s
getMoi = Moi $ \s -> (s,s)

putMoi :: s -> Moi s ()
putMoi s = Moi $ \_ -> ((),s)

exec :: Moi s a -> s -> s
exec (Moi sa) s = snd $ sa s

eval :: Moi s a -> s -> a
eval (Moi sa) s = fst $ sa s


modifyMoi :: (s -> s) -> Moi s ()
modifyMoi f = Moi $ \s -> ((),f s) 

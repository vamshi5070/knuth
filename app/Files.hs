main = do
  s <- readFile "Files.hs"
  doSomethingWith s

doSomethingWith :: String -> IO ()
doSomethingWith str = putStrLn str

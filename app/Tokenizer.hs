module Tokenizer where

import Control.Monad
import Control.Applicative
import Data.Char

-- newtype Parser  s a = Parser {parse :: s -> Maybe (a,s)}
newtype Parser a = Parser {parse :: String -> Maybe (a,String)}

runParser :: Parser a -> String -> a
runParser parser str = case parse parser str of
                    Nothing -> error "Parser error"
                    Just (x,"") -> x
                    _ -> error "Incomplete parse"

failure :: Parser a
failure = Parser $ const Nothing

instance Functor Parser where
  fmap f parser = Parser (\str -> case parse parser str of
                                    Nothing -> Nothing
                                    Just (x,reStr) -> Just (f x,reStr))

instance Applicative Parser where
  pure a = Parser (\str -> Just (a,str))
  (<*>) functionParser parser = Parser (\str -> case parse functionParser str  of
                                             Nothing  -> Nothing
                                             Just (f,reStr) ->  case parse parser reStr of
                                                                Nothing -> Nothing
                                                                Just (x,str') -> Just (f x,str'))
instance Monad Parser where
  return = pure
  (>>=) parser aRb = Parser (\str -> case  parse parser str of
                                   Nothing -> Nothing
                                   Just (x,str') -> parse (aRb x) str'
                               )

instance Alternative Parser where
  empty = failure
  (<|>) = option

instance MonadPlus Parser where
  mzero = failure
  mplus = (<|>)

--takes two parsers and returns first parser if it is valid ,  else returns second irrespective of the result
option :: Parser a -> Parser a -> Parser a
option p q = Parser (\str -> case parse p str of
                            Nothing -> parse q str
                            res -> res)

--gives head of the string ex: parse item "rgv"  = Just ('r',"gv")
item :: Parser Char
item = Parser helper
       where helper "" = Nothing
             helper (x:xs) = Just (x,xs)

-- head char if satisifies the condition returns it else returns failure
satisfy :: (Char -> Bool) -> Parser Char
satisfy predicate = do
  ch <- item
  if predicate ch
    then return ch
    else failure
  -- item >>= (\s -> if p s then return s else failure)

-- one of the given list of chars should be the head of the string
oneOf :: [Char] -> Parser Char
-- oneOf str = satisfy (flip elem str)
oneOf str = satisfy (`elem` str)

chainl :: Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = (p `chainl1` op) <|> return a

chainl1 :: Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl1` op = do
  a <- p
  rest a
  where rest a = (do f <- op
                     b <- p
                     rest (f a b)) <|> return a

char  :: Char -> Parser Char
char c = satisfy (c ==)

string :: String -> Parser String
string = mapM char
-- string "" = return []
-- string (c:cs) = do
  -- _ <- char c
  -- _ <- string cs
  -- return (c:cs)

spaces :: Parser String   -- removes whitespaces
spaces = many $ oneOf "\n\r "

preToken :: Parser a -> Parser a               -- removes whitespaces first , then parses the string
preToken p = spaces >> p
  -- _ <- spaces
  -- p

posToken :: Parser a -> Parser a                 -- first  parses the string then removes whitespaces
posToken p = do
  a <- p
  _ <- spaces
  return a

inToken :: Parser a -> Parser a              -- removes the whitespaces before and after parsing
inToken p = do
  _ <- spaces
  a <- p
  _ <- spaces
  return a

digit :: Parser Char
digit = satisfy isDigit

natural :: Parser Integer
natural  = read <$> some (satisfy isDigit)

number :: Parser Int
number = do
  sign <- string "-" <|> return []
  num <- some digit
  return $ read $ sign <> num

reserved :: String -> Parser String
reserved  = inToken . string

parens :: Parser a -> Parser a
parens p = do
  _ <- reserved "("
  str <- p
  _ <- reserved ")"
  return str

data Expr =
  Add Expr Expr
  | Mul Expr Expr
  | Sub Expr Expr
  | Lit Int
  deriving (Eq,Show)

eval :: Expr -> Int
eval (Lit val) = val
eval (Sub x y) = eval x - eval y 
eval (Add x y) = eval x + eval y
eval (Mul x y) = eval x * eval y

int :: Parser Expr
int = Lit <$> number
  -- num <- number
  -- return $ Lit num

infixOp :: String -> (a -> a -> a) -> Parser (a -> a -> a)
infixOp str op = reserved str >> return op

addOp :: Parser (Expr -> Expr -> Expr)
addOp = infixOp "+" Add <|> infixOp "-" Sub

mulOp :: Parser (Expr -> Expr -> Expr)
mulOp = infixOp "*" Mul

factor :: Parser Expr
factor = inToken int <|> parens expr

expr :: Parser Expr
expr = term `chainl1` addOp

term :: Parser Expr
term = factor `chainl1` mulOp

run :: String -> Expr
run = runParser expr

identifierBody :: Parser String
identifierBody =  many $ satisfy isAlphaNum <|> oneOf "_'"

identifierHead :: Parser Char
identifierHead = satisfy isLower

identifyParser :: Parser String
identifyParser = do
  name <- identifier
  _ <- reserved "="
  -- x <- expr
  return name

identifier :: Parser String
identifier = do
  c <-  identifierHead
  str <-  identifierBody 
  return (c:str)

-- data Identifier a =
--   Identifier (String ,a)

-- returner str = case parse identifier str of
                 -- Nothing -> Nothing
                 -- Just (x,str) -> Just (x,parse)

-- global = []

dataRun :: String ->  [Maybe (String,String)] -> Maybe Int
dataRun _ [] = Nothing 
dataRun key (Nothing:xs) = dataRun key xs
dataRun key (Just(x,str):xs)  
  | x == key = Just $ eval $ run  str
  | otherwise = dataRun key xs

main' :: IO ()
main' = do
  name <- getLine
  putStrLn $ maybe "Variable not found"  show (dataRun name [Nothing,Just ("x","1*34 *(34 + 44)-  40 - 10  "),Just ("name","1+34678-25")])

-- storrer :: String -> [] -> [String]
storrer :: [Char] -> [Maybe (String, String)] -> [Maybe (String, String)]
storrer str xs = name:xs
  where name = parse identifyParser str

main :: IO ()
main = forever $ do
  -- putStr "Enter an expression: "
  -- exp <- getLine
  s <- readFile "first.kh"
  let arr = storrer s []
  putStr "Enter a value: "
  var <- getLine
  -- putStr "Expression is, "
  putStrLn $ maybe "Variable not found" show (dataRun var arr)

main'' :: IO ()
main'' = do
  s <- readFile "first.kh"
  doSomethingWith s

doSomethingWith :: String -> IO ()
doSomethingWith = putStrLn

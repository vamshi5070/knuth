newtype State' s a =
  State' { runState' :: s -> (a , s) }

instance Functor (State' s) where
  fmap f sa = State' (\x -> let (a, s) = runState' sa x
                                in (f a,s))

instance Applicative (State' s) where
  pure x = State' (\s -> (x,s))
  (<*>) (State' fa) (State' sa) = State' (\s -> let (f,s') = fa s
                                                    (x,s'') = sa s'
                                                    in (f x,s'')
                                         )

instance Monad (State' s) where
  return = pure
  m >>= k = State' (\s ->  let (a,s') = runState' m s
                               in runState' (k a) s'
                   )

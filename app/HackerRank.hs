import Data.List

grouper :: (Eq a) =>  [a] -> [[a]]
grouper  xs = foldl (\acc x -> inserter x acc) [] xs

inserter :: (Eq a) =>  a -> [[a]] -> [[a]]
inserter x [] = [[x]]
inserter x (xs:xss) = if head xs == x then (x:xs):xss else xs: inserter x xss

worker xs = map (\x -> (head x,length x)) xs
-- main = interact $ map fst . filter (\(_,n) -> n >= 2) . worker . group  [4, 5, 2, 5, 4, 3, 1, 3, 4] 
ssr =  foldr (+) 0 . map (*2) . filter even 


recs :: Int -> IO ()
recs 0  = return ()
recs n  = do
  first <- getLine
  let firster = lister first
  -- map read . tail .  words . g
  second <- getLine
  let seconder = lister second
  print $ seconder
  recs $ n-1

lister :: String -> [Int]
lister = map read . words

mapper :: [Int] -> [Int]
mapper =  map fst . filter (\(_,k) -> k >= 2) . worker . grouper

dister [] = "-1" 
dister xs = makeStringer xs

-- makeStringer [] = ""
makeStringer :: [Int] -> String
makeStringer  = concat . intersperse " " . map show 

-- isEqualCount :: (Eq a) => a -> a -> [a]
-- isEqualCount x y xs = cntx == cnty
--   where
--     (cntx,cnty) = 
--       foldr (\ele (a,b)  ->
--                if ele == x
--                   then x 

isEqualCount :: (Eq a) => a -> a -> [a] -> Bool
isEqualCount x y xs = cntx == cnty
  where cntx = (length . filter (== x)) xs
        cnty = (length . filter (== y)) xs

isAbs1 :: (Eq a) => a -> a -> [a] -> Bool
isAbs1 x y xs = abs (cntx - cnty) <= 1
  where cntx = (length . filter (== x)) xs
        cnty = (length . filter (== y)) xs

prefixes :: [a] -> [[a]]
prefixes xs = map reverse $ tail $ scanr (:) [] xs

main = do
  let str = "RGGR"
  if (isEqualCount 'R' 'G' str)
    && (isEqualCount 'Y' 'B' str)
    && and ( map (isAbs1 'R' 'G') $ prefixes str)  
    && and ( map (isAbs1 'Y' 'B') $ prefixes str)
    then print True
    else print False
  

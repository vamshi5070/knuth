{-# LANGUAGE TypeApplications#-}


import Control.Monad.Trans.Class (lift)

newtype MaybeT m a =
  MaybeT { runMaybeT :: m (Maybe a)}

instance (Functor f) => Functor (MaybeT f)  where
  fmap f (MaybeT a) = MaybeT $ (fmap . fmap)  f a

instance (Applicative f) => Applicative (MaybeT f)  where
  pure a = MaybeT $ (pure . pure) a
  (<*>) (MaybeT f) (MaybeT a) = MaybeT $ (<*>) <$> f <*> a

-- instance Monad m => Monad (MaybeT m) where
--   return  = pure
--   -- (>>=) (IdentityT a) f = IdentityT $ a >>= runIdentityT . f
--   (>>=) (MaybeT Nothing) f = return Nothing
--   (>>=) (MaybeT (Just n)) f = f n
--   -- (>>=) ma f = MaybeT $ aimb
--   --   where aimb = do
--   --           val <- runMaybeT ma
--   --           case val of
--   --             Nothing -> return Nothing
--   --             Just n ->  (runMaybeT $  f n)
--   --           -- join $ fmap (runIdentityT  . f) (runIdentityT ma)

-- myPrompt :: String -> IO String
-- myPrompt prompt = do
--   putStr prompt
--   getLine

-- -- getName :: MaybeT IO String
-- -- getName = do
--   -- input <- lift $ myPrompt "Name: "
--   -- if input == ""
--     -- then MaybeT $ return Nothing
--     -- else MaybeT $ return $ Just $ input

-- maybe' ::  b -> (a -> b) -> Maybe a -> b
-- maybe' y f Nothing = y
-- maybe' y f (Just x) = f x

-- maybeT' :: (Monad m) => m b -> (a -> m b) -> MaybeT  m a -> m b
-- maybeT' y f rgv = do
--   rgv' <- runMaybeT rgv
--   case  rgv' of
--     Nothing ->  y
--     (Just x) -> f x

-- either' :: (a -> c) -> (b -> c) -> Either a b -> c
-- either' f g (Left x) = f x
-- either' f g (Right x) = g x

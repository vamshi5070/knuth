-- import Data.Char
import Tokenizer
import Control.Monad

main = forever $  do
  putStr "> "
  a <- getLine
  print $ eval $ run $ a

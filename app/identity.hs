import Control.Monad

main :: IO ()
main = return ()

newtype Identity a =
  Identity {runIdentity :: a}
  deriving (Eq,Show)

newtype IdentityT  f a =
  IdentityT {runIdentityT :: f a}
  deriving (Eq,Show)

instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Functor f => Functor (IdentityT f) where
  fmap f (IdentityT a) = IdentityT $ f <$> a

instance Applicative Identity where
  pure = Identity
  (<*>) (Identity f) (Identity a) = Identity $ f a

instance Applicative f =>
  Applicative (IdentityT f) where
  pure a = IdentityT $ pure a
  (<*>) (IdentityT f) (IdentityT a) = IdentityT $ f <*> a

instance Monad Identity where
  return  = pure
  (>>=) (Identity a) f = f a

instance Monad m => Monad (IdentityT m) where
  return  = pure
  -- (>>=) (IdentityT a) f = IdentityT $ a >>= runIdentityT . f
  (>>=) ma f = IdentityT aimb
    where aimb = join $ fmap (runIdentityT  . f) (runIdentityT ma)

import Data.List

seven :: Integer -> (Integer, Int)
seven 7 = (1,2)

helperSeven x
  | x <= 7 = [x]
  | otherwise = []

data TreeNode = Node TreeNode Int TreeNode
              | Leaf Int
              | None
              deriving (Eq,Show)

tree1 = Node (Node (Leaf 9) (-22) (Leaf 50)) 5 (Node (Leaf 9) 11 (Leaf 2))

folder :: Int -> [Int] -> TreeNode -> [Int]
folder acc xs (Leaf num) = (acc+num):xs
folder acc xs (Node l num r) = folder acc' (folder acc' xs l) r
  where acc' = acc+num

helper :: TreeNode -> Int
helper = maximum . folder 0 []


vamtree = Node (Leaf (-1)) (-1) (Node None 0 None)

cookingTime :: Integer -> Integer
cookingTime num
  | num `mod` 8 == 0 = num `div` 8 * 5
  | otherwise = (num `div` 8 + 1) * 5

mainDiagonalProduct :: Num a => [[a]] -> a
mainDiagonalProduct mat = go 0 1 mat
  where go index acc [] = acc
        go index acc (xs:xss) = go (index+1) (acc*(xs!! index)) xss


shortcut :: String -> String
shortcut = filter (not . (flip elem "aeiou"))

minimumSteps :: [Int] -> Int -> Int
minimumSteps = go 0 0
  where go cnt acc xs key
          | acc' >= key = cnt
          | otherwise = go (cnt+1) acc' xs' key
            where x = minimum xs
                  acc' = x + acc
                  xs' = delete x xs

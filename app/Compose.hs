newtype Identity a =
  Identity { runIdentity :: a }

main = return ()

instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Applicative Identity where
  pure = Identity
  (<*>) (Identity f) (Identity a) = Identity $ f a

instance Monad Identity where
  return = pure
  (>>=) (Identity a) aRb =  aRb a

flip' :: (a -> b -> c) -> b -> a -> c
flip' f x y = f y x

newtype One f a =
  One (f a)
  deriving (Eq, Show)

instance Functor f =>
  Functor (One f) where
  fmap f (One fa) = One $ fmap f fa

instance Applicative f =>
  Applicative (One f) where
  pure a = One $ pure a
  (<*>) (One f) (One a) = One $ f <*> a

instance Monad m => Monad (One m) where
  return = pure
  (>>=) (One a) aRb = One $ a >>= aRb

newtype Compose f g a =
  Compose { getCompose :: f (g a) }
                      deriving (Eq, Show)

instance (Functor f ,Functor g) => Functor (Compose f g)  where
  fmap f (Compose a) = Compose $ (fmap . fmap)  f a

instance (Applicative f ,Applicative g) => Applicative (Compose f g)  where
  pure a = Compose $ (pure . pure) a
  (<*>) (Compose f) (Compose a) = Compose $ (<*>) <$> f <*> a

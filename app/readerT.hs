newtype ReaderT r m a =
  ReaderT { runReaderT :: r -> m a }

instance (Functor f) => Functor (ReaderT r f) where
  fmap f (ReaderT ra) = ReaderT (\r -> fmap f (ra r))

instance (Applicative f) => Applicative (ReaderT r f) where
  pure a = ReaderT $ const $ pure a
  (<*>) (ReaderT fa) (ReaderT ra) = ReaderT $ (<*>) <$> fa <*> ra

instance (Monad m) => Monad (ReaderT r m) where
  return = pure
  m >>= k = ReaderT aimb
    where aimb x =  do
          m' <- runReaderT m x
          runReaderT (k m') x 

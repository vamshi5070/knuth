data Op = Add | Sub | Mul | Div
          deriving (Eq,Show)

valid :: Op -> Int -> Int -> Bool
valid Add _ _ = True
valid Sub x y = x > y
valid Mul _ _ = True
valid Div x y = x `mod` y == 0

apply :: Op -> Int -> Int -> Int
apply Add x y = x + y
apply Sub x y = x - y
apply Mul x y = x * y
apply Div x y = x `div` y

data Expr = Val Int | App Op Expr Expr
          deriving Show

values :: Expr -> [Int]
values (Val n) = [n]
values (App _ l r) = values l ++ values r

eval :: Expr -> Maybe Int
eval (Val n)
  | n > 0 = Just n
  | otherwise = Nothing

eval (App o l r) = do
  x <- eval l
  y <- eval r
  if valid o x y
    then
    return $ apply o x y
    else Nothing

interleave :: a -> [a] -> [[a]]
interleave x [] = [[x]]
interleave x (y:ys) = (x:y:ys) : (map (y:) $ interleave x ys)

perms :: [a] -> [[a]]
perms [] = [[]]
perms (x:xs) = concatMap (interleave x) (perms xs)

subs :: [a] -> [[a]]
subs [] = [[]]
subs (x : xs) = yss ++ map (x:) yss
  where yss = subs xs

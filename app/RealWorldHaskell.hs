
-- file: ch03/ListADT.hs
data List a = Cons a (List a)
            | Nil
              deriving (Show)

consToList Nil = [] 
consToList (Cons x xs) = x:consToList xs

listToCons [] = Nil
listToCons (x:xs) = Cons x $ listToCons xs

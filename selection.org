* First stage
** Minimum
#+begin_src haskell
minimum :: Ord a => [a] -> a
minimum [] = error "empty list"
minimum (x:xs) = min x  (minimum' xs)
#+end_src 

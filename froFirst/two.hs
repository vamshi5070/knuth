isPalindrome :: String -> Bool
isPalindrome word =
  word == reverse word

nonEmptyPal :: String -> Bool
nonEmptyPal "" = False
nonEmptyPal word = isPalindrome word

main :: IO ()
main = do
  word <- getLine
  print $ nonEmptyPal word

minimum' :: Ord a => [a] -> a
minimum' [] = error "empty list"
minimum' [x] = x
minimum' (x:xs) = min x  (minimum' xs)

maximum' :: Ord a => [a] -> a
maximum' = foldr1 max

minmax :: Ord a => [a] -> (a,a)
minmax [] = error "empty list"
minmax (x:xs) = foldr helperMinMax (x,x) xs 
	where helperMinMax x (minV,maxV) 
		| x < minV = (x,maxV)
		| x > maxV = (minV,x)
		| otherwise = (minV,maxV)

-- select n xs  = 

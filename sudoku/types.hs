import Data.List ( transpose )

type Grid = Matrix Value
type Matrix a = [Row a]
type Row a  = [a]
type Value = Char

blank :: Grid
blank = replicate 9 (replicate 9 '.')

rows :: Matrix a -> [Row a]
rows = id

-- transpose :: Matrix a -> [Row a]
-- transpose [] = []
-- transpose ([]:xss) = transpose xss
-- transpose xss = (map head xss ) : transpose (map tail xss)

cols :: Matrix a -> [Row a]
cols = transpose

boxsize = 3

boxs :: Matrix a -> [Row a]
boxs = unpack . map transpose . pack
  where pack  = split . map split
        unpack = map concat . concat
        split = chop boxsize

chop :: Int -> [a] -> [[a]]
chop _ []  = []
chop n xs  = take n xs: chop n (drop n xs) 


valid :: Grid  -> Bool
valid g = all nodups (rows g) && all nodups (cols g) && all nodups (boxs g)

nodups :: Eq a => [a] -> Bool
nodups [] = True
nodups (x:xs) = not (x `elem` xs) && nodups xs

* Greedy
  The point of greedy algorithm is selecting minimum candidate in each encountered step.
* List
The list data structure can be optimized with greedy algorithm technique.
#+begin_src haskell
mcc :: [Component] -> Candidate
mcc = minWith cost . candidates
#+end_src
* Minwith
#+begin_src haskell :results output
:{
minWith :: (Ord b) => (a -> b) -> [a] -> a
minWith f = foldr1 (smaller f)
   where smaller f x y 
	       | f x < f y = x
	       | otherwise = y
 :}
#+end_src

#+RESULTS:
: Prelude| Prelude| Prelude| Prelude| Prelude| Prelude| 
: <interactive>:90:1-8: warning: [-Wtabs]
:     Tab character found here, and in one further location.
:     Please use spaces instead.

* Candidates
#+begin_src haskell
candidates :: [Component] ->  [Candidates]
candidates  xs = foldr step [co] xs
                 where step x cs = (concatMap . extend x) xs
#+end_src		 

* Greedy sorting algorithm
Filter the permutation with zero inversion cost.
#+begin_src haskell
sort :: Ord a => [a] -> [a]
sort  = minWith ic . perms
#+end_src		 

* Inversion cost
The inversion cost is the number of pairs of consecutive numbers in a list which satisfy the condition of first element is greater than second.
#+begin_src haskell
:{
ic :: Ord a => [a] -> Int
ic = length . filter (\(x,y) -> x > y)  . pairs
:}
ic [7,1,2,3]
#+end_src

#+RESULTS:
: 3

* Pairs
List of tuples with consective numbers each from a given list.
#+begin_src haskell
:m Data.List
:{
pairs ::  (Ord a) => [a] -> [(a,a)]
pairs xs = do 
    x:ys <- tails xs
    y:zs <- tails ys
    return $ (x,y)
:}
pairs [1,2,3]

#+end_src

#+RESULTS:
: Prelude Data.List| Prelude Data.List| Prelude Data.List| Prelude Data.List| Prelude Data.List| Prelude Data.List| Prelude Data.List> [(1,2),(1,3),(2,3)]

* Perms
All possible permutations , npn.
#+begin_src haskell
:{
perms :: [a] -> [[a]]
perms = foldr step [[]]
   where step = concatMap . extend 
:}  
#+end_src
* Extend
#+begin_src haskell
:{
extend :: a -> [a] -> [[a]]
extend x [] = [[x]]
extend x (y:ys) = (x:y:ys):map (y:) (extend x ys)
:}  
#+end_src

#+RESULTS:

* Greedy step
*gstep* is used for finding a optimized solution of problem at each step.
#+begin_src haskell
  :{
  gstep x = minWith ic . extend x
  :}  
#+end_src

#+RESULTS:
* Greedy step for sort
#+begin_src haskell
  :{
gstep :: (Ord a) => a -> [a] -> [a]
gstep ele [] = [ele]
gstep ele (x:xs) 
  | ele <= x = ele:x:xs
  | otherwise = x: gstep ele xs
  :}  
#+end_src

#+RESULTS:

import Data.List

pairs ::  (Ord a) => [a] -> [(a,a)]
pairs xs = do 
    x:ys <- tails xs
    y:zs <- tails ys
    return $ (x,y)

ic :: Ord a => [a] -> Int
ic = length . filter (\(x,y) -> x > y)  . pairs

perms :: [a] -> [[a]]
perms = foldr step [[]]
   where step = concatMap . extend 

extend :: a -> [a] -> [[a]]
extend x [] = [[x]]
extend x (y:ys) = (x:y:ys):map (y:) (extend x ys)

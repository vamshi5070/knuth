import Test.QuickCheck

-- ghcid  "--command=ghci fileName.hs"
data Tree a = Leaf a | Node (Tree a) (Tree a)
           deriving Show

size :: Tree a -> Int
size (Leaf _) = 1
size (Node l r) = size l + size r

height :: Tree a -> Int
height (Leaf _) = 1
height (Node l r)  = 1 + max (height l) (height r)

tree :: Tree Int
tree =  ( (Leaf 3) `Node` (Leaf 4)) `Node`  (Leaf 11)
-- tree = Node (Node (Leaf 3) (Leaf 4)) (Leaf 11)
  {-
      --   /\
      --  /\ 11
      -- 3  4
  -}
tree1 :: Tree Int
tree1 = Node (Node (Leaf 13) (Leaf 4)) (Leaf 0)
{-
      --   /\
      --  /\ 0
      -- 13  4
-}

tree2 :: Tree Int
tree2 = Node  (Leaf 13) (Leaf 4)
{-
      --  /\ 
      -- 13  4
-}
tree3 :: Tree Int
tree3 =   (Leaf 13) 
{-
      -- 13  
-}
tree4 :: Tree Int
tree4 =   (Leaf (-113))
{-
      -- -113  
-}
  
megaTree = rollup [tree,tree1,tree2,tree3,tree4]
{-
step1:-

     /\
    /\
   /\ 11
  3 4
step2:-
       /\
      /  \
     /    \
    /\    /\
   /\ 11 /\ 0
  3 4   13 4

step3:-
       /\
      /  \
     /    \
    /\    /\
   /\ 11 /\ 0
  3 4   13 4
 /\
13 4

step4:-
         /\
        /  \
       /    \
      /      \
     /\      /\
    /\ 11   /\ 0
   3   4   13 4
  /\   /\
 13 4 13 -113

-}
fringe :: Tree a -> [a] -- list of leaves , note: always a non empty list
fringe (Leaf x) = [x]
fringe (Node l r) = fringe l <> fringe r

unwrap :: [a] -> a
unwrap [x] = x
unwrap _ = error "not a singleton list"

single :: [a] -> Bool
single [_] = True
single _ = False

pairWith f [] = []
pairWith f [x] = [x]
pairWith f (x:y:xs) = f x y : pairWith f xs

until' :: (a -> Bool) -> (a -> a) -> a -> a
until' p f x
  | p (f x) = f x
  | otherwise = until' p f (f x)

mkTree :: [a] -> Tree a
mkTree = unwrap . until' single (pairWith Node) . map Leaf

extendTree :: a -> Tree a -> [Tree a]
extendTree x (Leaf y) = [Node (Leaf x) (Leaf y)]
extendTree x (Node u v) =
  [Node (Leaf x) (Node u v)] <> (map (\y -> Node y v) res  )
  where res = (extendTree x u)

cost :: (Ord a,Num a) => Tree a -> a
cost (Leaf x) = x
cost (Node u v) = 1 + cost u `max` cost v

mkTrees :: [a] -> [Tree a]
mkTrees [x] = [Leaf x]
mkTrees (x:xs) = concatMap (extendTree x) (mkTrees xs)

foldrn :: (a -> b -> b) -> (a -> b) -> [a] -> b
foldrn f g [x] = g x
foldrn f g (x:xs) =  f x (foldrn f g xs)

wrap :: a -> [a]
wrap x = [x]

mkTrees' :: [a] -> [Tree a]
mkTrees' = foldrn (concatMap . extendTree) (wrap . Leaf)

type Forest a = [Tree a]

rollup :: [Tree a] -> Tree a
rollup = foldl1 Node

spine :: Tree a -> Forest a 
spine (Leaf x) = [Leaf x]
spine  (Node u v) = spine u ++ [v]

extendForest :: a -> Forest a -> [Forest a]
extendForest x ts  = [Leaf x:rollup (take k ts):drop k ts | k <- [1..length ts]]

mkForests :: [a] -> [Forest a]
mkForests = foldrn (concatMap . extendForest) (wrap . wrap  . Leaf)

mkTrees'' ::  [a] -> [Tree a]
mkTrees'' = map rollup . mkForests
 
join :: (Ord a, Num a) => a -> Forest a -> Forest a
join x [u] = [u]
join x (u:v:ts)
  | x `max` cost u > cost v = u:v:ts
  | otherwise = join x (Node u v:ts)

add x ts = Leaf x:join x ts


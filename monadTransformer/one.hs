import Data.Char
import Control.Monad
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Class

getPassphrase :: MaybeT IO String
getPassphrase = do
  s <- lift getLine
  guard (valid s)
  return s

--     -- then return $ Just s
--     -- else return Nothing

-- -- getPassphrase :: IO (Maybe String)
-- -- getPassphrase = do
-- --   s <- getLine
-- --   if valid s
-- --     then return $ Just s
-- --     else return Nothing

valid :: String -> Bool
valid str = length str >= 8
            && any isAlpha str
            && any isNumber str
            && any isPunctuation str

-- askPassphrase :: IO ()
-- askPassphrase = do
--   putStrLn "Enter something"
--   maybe_value <- getPassphrase
--   case maybe_value of
--     Just value -> do putStrLn $ "password is, "  <> value -- do stuff
--     Nothing -> putStrLn "password invalid"

askPassphrase :: MaybeT IO ()
askPassphrase = do
  lift $ putStrLn "Enter something"
  value <- getPassphrase
  lift $ putStrLn "Entering database"

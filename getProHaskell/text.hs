{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Data.Semigroup

firstWord :: String
firstWord = "pessimism"

secondWord :: T.Text
secondWord = T.pack firstWord

thirdWord :: String
thirdWord = T.unpack secondWord 

fourthWord :: T.Text
fourthWord = T.pack thirdWord

classy :: T.Text
classy = "xmonad"

fascal = "waqt " <> " " <> " ne kiya"

search = "धर्म"

mainText = "श्रेयान्स्वधर्मो विगुणः परधर्मात्स्वनुष्ठितात्। स्वधर्मे निधनं श्रेयः परधर्मो भयावहः"

highlight :: T.Text -> T.Text -> T.Text
highlight query fullText = T.intercalate highlighted pieces
  where
  highlighted = mconcat ["{", query, "}"]
  pieces = T.splitOn query fullText

main = do
  TIO.putStrLn $ highlight search mainText

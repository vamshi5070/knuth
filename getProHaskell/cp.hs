import System.Environment

main :: IO ()
main = do
  args <- getArgs
  if length args /= 2
    then error "enter correct no.of args"
    else execMain args

execMain :: [String] -> IO ()
execMain [oldFile,newFile]  = do
  content <- readFile oldFile
  writeFile newFile content


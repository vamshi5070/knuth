import Control.Monad
import System.Environment

main = do
  args <- getArgs
  let linesToRead = if length args > 0
        then read (head args)
        else 0
  numbers <- replicateM linesToRead getLine
  putStrLn "Sum goes here.."
  print . sum . (map read) $ numbers
       
myReplicateM :: Monad m => Int -> m a -> m [a]
myReplicateM n func = mapM (\_ -> func) [1 .. n]


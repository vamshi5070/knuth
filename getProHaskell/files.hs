{-# LANGUAGE OverloadedStrings #-}
import System.IO
import System.Environment
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

main = do
  args <- getArgs
  let fileName = head args
  input <- TIO.readFile fileName
  let summary = (countsText . getCounts) input
  TIO.appendFile "stats.dat" $ mconcat [(T.pack fileName), " " , summary , "\n"]
  TIO.putStrLn summary

getCounts :: T.Text -> (Int,Int,Int)
getCounts input = (charCount, wordCount, lineCount)
  where charCount = T.length input
        wordCount = (length . T.words) input
        lineCount = (length . T.lines) input

countsText ::  (Int,Int,Int) -> T.Text
countsText (charCount, wordCount, lineCount) =
  T.pack $ unwords ["chars: " , show charCount
          , "words: " , show wordCount
          , "lines: " , show lineCount
          ]

xmonad = "rgv"


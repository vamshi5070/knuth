main :: IO ()
main = return ()

data Stack a = Empty
  | Stack a (Stack a)
  deriving (Eq,Show)

push :: a -> Stack a -> Stack a
push = Stack 

pop :: Stack a -> (a,Stack a)
pop Empty = error "empty stack"
pop (Stack x rest) = (x,rest)



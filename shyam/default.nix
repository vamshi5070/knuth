{ mkDerivation, lib }:
mkDerivation {
  pname = "shyam";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  license = "unknown";
  hydraPlatforms = lib.platforms.none;
}

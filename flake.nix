{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
          haskell-env = pkgs.haskellPackages.ghcWithHoogle( hp: with hp; [
            hspec          
           #  random
           # parsec_3_1_15_0
           #monad-loops
	   #      unlit
           #scotty
           #parallel
           #split
           #readline
            #Euterpea
            #HSoM
            # yesod
            # stack2nix
          ]);
          miscPkgs =  with pkgs;
            [ #cabal-install
              ghcid
              #clisp
              #cabal2nix-unwrapped
              #stack
              #haskell-language-server
            ];
      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            haskell-env
          ] ++ miscPkgs ;
            };
      });
}

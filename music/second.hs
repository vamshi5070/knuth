import Euterpea
import HSoM.Examples.Interlude (childSong6)

main :: IO ()
main = return ()

transM ap (Prim (Note d p)) = Prim (Note d (trans ap p ))
transM _ (Prim (Rest d)) = Prim (Rest d)
transM ap (m1 :+: m2) = transM ap m1 :+: transM ap m2
transM ap (m1 :=: m2) = transM ap m1 :=: transM ap m2
transM ap (Modify patter m1) =  Modify patter (transM ap m1)
-- transM ap (Modify (Transpose ab ) m1) =  (Modify (Transpose ab ) (transM ap m1))
-- transM ap (Modify (Instrument ins ) m1) =  (Modify (Transpose ab ) (transM ap m1))


mel' :: Music (PitchClass, Octave)
mel' = note en p1  :+:
  note qn p2  :+:
  note qn p3  :+: note en p1 
  where p1 = (Aff,2)
        p2 = (Bs,2)
        p3 = (C,2)

created =  instrument  Vibraphone    mel'  :=: instrument Cello  mel'

rgv :: Music (PitchClass, Octave)
rgv =  note bn (Aff ,1) :=:  initial :+: note bn (Fff , 1)  :+: last
  where
    initial =  line (replicate 5 created)
    last = created :+: created :+: created :+: created' :=: created :+: created' :=: created

created' =  instrument  SlapBass1   mel'  :=: instrument Cello  mel'

twinkle = c 4 qn :+: c 4 qn :+: g 4 qn :+: g 4 qn :+: a 4 qn :+: a 4 qn :+: g 4 hn :+: f 4 qn :+: f 4 qn :+: e 4 qn :+: e 4 qn :+: d 4 qn :+: d 4 qn :+: c 4 hn :+: g 4 qn :+: g 4 qn :+: f 4 qn :+: f 4 qn :+: e 4 qn :+: e 4 qn :+: d 4 hn :+: g 4 qn :+: g 4 qn :+: f 4 qn :+: f 4 qn :+: e 4 qn :+: e 4 qn :+: d 4 hn :+: c 4 qn :+: c 4 qn :+: g 4 qn :+: g 4 qn :+: a 4 qn :+: a 4 qn :+: g 4 hn :+:  f 4 qn :+: f 4 qn :+: e 4 qn :+: e 4 qn :+: d 4 qn :+: d 4 qn :+: c 4 hn

prefixes :: [a] -> [[a]]
prefixes [] = []
prefixes (x : xs) =  [x] : map f (prefixes xs)
  where f pf = x : pf

prefix :: [Music a] -> Music a
prefix mel = let m1 = line (concat (prefixes mel))
                 m2 = transpose 12 . line . concat . prefixes $ reverse mel
                 m = instrument Flute m1 :=: instrument VoiceOohs m2
             in m :+: transpose 5 m :+: m

mel2 = [c 5 sn, e 5 sn, g 5 sn, b 5 sn, a 5 sn, f 5 sn, d 5 sn, b 4 sn, c 5 sn]

pcToQN :: PitchClass -> Music Pitch
pcToQN pc = note qn (pc, 4)

twinkle1 = line (map pcToQN [C, C, G, G, A, A]) :+: g 4 hn
  :+: line (map pcToQN [F, F, E, E, D, D]) :+: c 4 hn
  :+: line (map pcToQN [G, G, F, F, E, E]) :+: d 4 hn
  :+: line (map pcToQN [G, G, F, F, E, E]) :+: d 4 hn
  :+: line (map pcToQN [C, C, G, G, A, A]) :+: g 4 hn
  :+: line (map pcToQN [F,F,E,E,D,D]) :+: c 4 hn :+:
  line (map pcToQN [F, F, E, E, D, D]) :+: c 4 hn

twinkle2 =  let m1 = line (map pcToQN [C, C, G, G, A, A]) :+: g 4 hn
                m2 = line (map pcToQN [F, F, E, E, D, D]) :+: c 4 hn
                m3 = line (map pcToQN [G, G, F, F, E, E]) :+: d 4 hn
            in line [m1, m2, m3, m3, m1, m2]

newMusic = d 4 qn 
 

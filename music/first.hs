import Euterpea

main = return ()

mel = (note qn p1 :=: note qn (trans (-3) p1)) :+:
  (note qn p2 :=: note qn (trans (-3) p2)) :+:
  (note qn p3 :=: note qn (trans (-3) p3))
  where p1 = (Aff,2)
        p2 = (Bs,2)
        p3 = (C,2)

rgv :: Music (PitchClass, Octave)
rgv = note qn p1
  where p1 = (D,7)

mel' :: Music (PitchClass, Octave)
mel' = note en p1  :+:
  note qn p2  :+:
  note qn p3  :+: note en p1 
  where p1 = (Aff,2)
        p2 = (Bs,2)
        p3 = (C,2)

mel'' :: Music (PitchClass, Octave)
mel'' = note hn p1  :+:
  note qn p2  :+:
  note qn p3  :+: note qn p1 
  where p1 = (Aff,2)
        p2 = (Bs,2)
        p3 = (C,2)

silence = rest 0.07
leaked' = Modify  (Instrument Cello) (Modify (Transpose 1) (Modify (Tempo 1.1 ) mel'))

created =  instrument  Vibraphone    mel'  :=: instrument Cello  mel'
created' =  instrument  SlapBass1   mel'  :=: instrument Cello  mel'
 -- playDev 2 $ (note bn (Aff , 1))  :+: created :+: created :+:created :+: created :+: created :+: created :=: (note bn (Fff , 1))


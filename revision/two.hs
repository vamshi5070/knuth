even' :: Int -> Bool
even' 0 = True
even' n = odd' $ n-1

odd' :: Int -> Bool
odd' 0 = False
odd' n =  even' $ n-1

palindrome :: (Eq a) => [a] -> Bool
palindrome xs =  reverse xs  ==  xs

elem' :: (Eq a) => a -> [a] -> Bool
elem' _ [] = False
elem' key (x:xs)
  | key == x = True
  | otherwise = False

elem'' :: (Eq a) => a -> [a] -> Bool
elem'' key xs = foldr (\ x acc -> if x == key then True else acc) False xs
                
comb :: [a] -> [[a]]
comb [] = [[]]
comb xs = do
  x <- xs
  map (x:) $ comb $ tail xs

-- combN :: Int -> [a] -> [[a]]
-- combN _ [] = [[]]
-- combN n 

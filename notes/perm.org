* perm2 [1,2,3] 2

  perm2 [1,2,3] 2 = map (1:) perm2 [2,3] 1
  perm2 [2,3] 1 = map (2:) perm2 [3] 0
  perm2 [3] 0 = [[]]
  perm2 [2,3] 1 = map (3:) perm2 [] 0
  perm2 [] 0 [[]]
  
  [ [2] ,[3]]
  [ [1,2] , [1,3]]
  perm2 [1,2,3] 2 = map (2:) perm2 [1,3] 1
  perm2 [1,3] 1 = map (1:) perm2 [3] 1
  

* rgv 

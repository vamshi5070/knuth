data Optional a = None | Only a
  deriving (Eq,Show)

instance Semigroup a => Semigroup (Optional a) where
  None <> None = None
  (Only x) <> None = Only x
  None <> (Only y)  = Only y
  (Only x) <> (Only y) = Only $ x <> y

instance Monoid a => Monoid (Optional a) where
  mempty = None
  mappend = (<>)


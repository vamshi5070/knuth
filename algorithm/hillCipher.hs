import Data.List

-- transpose :: [[a]] -> [[a]]
-- transpose [] = []
-- transpose ([]:xss) = transpose xss
-- transpose xss = (map head xss):(transpose (map tail xss))

cipher ch =  lookup ch $ zip ['a'..'z'] [0..]

splitAt3 [] = []
splitAt3 xs = take 3 xs:(splitAt3 (drop 3 xs))

plainText :: String -> [[Maybe Int]]
plainText str = (\x -> [x]) <$> cipher <$> str

-- multiplyRowCol [] [] = 0
-- multiplyRowCol

addition xs ys
  | isSquare xs && isSquare ys && (length xs == length ys) = zipWith (zipWith (+)) xs ys
  | otherwise = []

isSquare xs = all (\x -> (length x) == length xs) xs

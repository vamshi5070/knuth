-- import Data.List
import Control.Applicative
import Data.Maybe

main = return ()

data Btree a = Empty
             | Node a (Btree a)  (Btree a)
             deriving (Eq,Show)

treeA = Node "b" (Node "d" Empty Empty) (Node "e" Empty Empty)
treeB = Node "q" (Node "f" Empty Empty) (Node "s" Empty Empty)

treeAB = Node "a" treeA treeB

paths :: Eq a => Btree a -> [[(a,Direction)]]
paths Empty = []
paths (Node x l r) = if null res
                     then [[(x,END)]]
                     else acc
  where res = paths l <> paths r
        acc = map ((x,L):) pl <> map ((x,R):) pr
        pl = paths l
        pr = paths r

data Direction = L | R | END deriving (Eq,Show)

-- path :: (Eq a) => a -> Btree a -> Maybe [a]
-- path ele tree = fmap ((\x -> x++ [ele] ) . (takeWhile (/=ele))) $ listToMaybe $  res
  --if res == [] then Nothing else Just $ head res
-- where res  =  filter (elem ele ) ( paths tree)

-- pathDir :: (Eq a) => a -> Btree a -> Maybe [a]
pathDir :: Eq a => a -> Btree a -> Maybe [(a, Direction)]
pathDir ele tree =  (++ [(ele,END)] ) . takeWhile (( /= ele) . fst ) <$> listToMaybe   res
  --if res == [] then Nothing else Just $ head res
  where res  =  filter (elem ele . fmap fst  ) ( paths tree)

-- count (Empty) = 0
-- count (Node x l r) = 1 + (count l) + (count r)

subTree :: (Eq a) => a -> Btree a  -> Maybe (Btree a)
subTree _ Empty = Nothing
subTree ele tree@(Node x l r)
  | ele == x = Just tree
  | otherwise = subTree ele l <|> subTree ele r

data PDirection = PLeft | PRight | NoParent deriving (Eq,Show)

type PPosition = Int

type Level = Int

-- tpprint :: PDirection -> [PPosition] -> Level -> Btree a -> [String]
-- tpprint _ _ _ (Empty) = []
-- tpprint pd pp level (Node x l r) =

level :: Btree a -> [(a,Int)]
level Empty = []
level (Node x Empty Empty) = [(x,1)]
level (Node x l r) = (x,1):((\(x,y) -> (x,y+1)) <$> res)
  where res = level l <> level r

levelOrd :: Btree a -> [a]
levelOrd = go . (:[])
  where go :: [Btree a] -> [a]
        go [] = []
        go (Empty:xs) = go xs
        go (Node x l r: xs) = x : go (xs ++ [l,r])

deg :: Btree a -> Bool
deg Empty = True
deg (Node _ l Empty) =  deg l
deg (Node _ Empty r) =  deg r
deg (Node _ l r) = False

full :: Btree a -> Bool
full Empty = True
full (Node _ Empty Empty) = True
full (Node _ l Empty) = False
full (Node _ Empty r) = False
full (Node _ l r) =  full l && full r

heightBST :: Btree a -> Int
heightBST Empty = 0
heightBST (Node  _ l r) = 1 + max (heightBST l) (heightBST r)

balancedTree :: Btree a -> Bool
balancedTree Empty = True
balancedTree (Node _ l r) = diff <= 1 && balancedTree l && balancedTree r
  where diff = abs $ heightBST l - heightBST r

-- count :: Btree a -> Int
count Empty = 0
count (Node _ l  r) = 1 + count l + count r

-- lEqR :: BinaryTree a -> Bool
-- lEqR Leaf = False
lEqR tree = 2 ^ heightBST tree - 1 == count tree

-- count nodes w\o leaves
cnt' :: Btree a -> Int
cnt' Empty = 0
cnt' (Node _ Empty Empty) = 0
cnt' (Node _ l r) = 1 + cnt' l + cnt' r

comp tree = 2 ^ (heightBST tree-1) - 1 == cnt' tree

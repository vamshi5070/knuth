import Data.List
import qualified Data.Map as M

newtype UnEdge a w = UnEdge (a,a,w)
                deriving (Show)

instance (Eq a ,Eq w) => Eq (UnEdge a w) where
  UnEdge (x,y,weight) == UnEdge (x1,y1,weight1) = (weight == weight1 ) && (x == x1) && (y == y1) || (y == x1) && (x == y1)

newtype Graph a w = Graph [UnEdge a w]
  deriving (Eq,Show)

vertices :: (Eq a) => Graph a w -> [a]
vertices (Graph xs) = nub $ xs >>= (\(UnEdge (x,y,_)) -> [x,y] )

myGraph :: Graph Char Int
myGraph = Graph [UnEdge ('a','b',1), UnEdge ('b','c',2) , UnEdge ('x','a',2),UnEdge ('b','z',3),UnEdge ('z','c',1),UnEdge ('a','w',1),UnEdge ('c','w',1)]

adj :: (Eq a) => Graph a w -> a -> [(a,w)]
adj (Graph xs) node = xs  >>= helper --xs >>= (\x -> helper x )
  where helper (UnEdge (x,y,weight))
          | x == node = [(y,weight)]
          | y == node = [(x,weight)]
          | otherwise = []

labell ::(Eq a) => Graph a w -> a -> [(a,Maybe w)]
labell graph start = [(node,Nothing) | (node,_) <- res]
  where res = adj graph start

starter :: (Eq a) => Graph a w -> [(a,Maybe w)]
starter graph = (\x -> (x,Nothing)) <$> vertices graph

data Btree a = Empty
  | Node a (Btree a) (Btree a)
  deriving (Eq,Show)

zipWith' :: [a] -> [a] -> [a]
zipWith' xs [] = xs
zipWith' [] xs = xs
zipWith' xs ys = zipWith (++) xs ys

tree1 = Node 2 Empty (Node 5 Empty Empty)
tree2 = Node 5 (Node 33 Empty Empty) (Node 52 Empty Empty)
treeRGV = Node 44 tree1 tree2
arr = Node 1 (Node 2 (Node 3 Empty Empty) (Node 4 Empty Empty)) (Node 2 (Node 4 Empty Empty) (Node 3 Empty Empty))

levelOrd Empty = [[]]
levelOrd (Node x Empty Empty) = [[x]]
levelOrd (Node x l r) = [x] : zipWith' (levelOrd l) (levelOrd r)

isSymmetric :: (Eq a) => [a] -> Bool
isSymmetric xs = vs == reverse us
  where (us,vs) = splitAtHalf xs
        splitAtHalf ys = splitAt (length ys `div` 2) ys

levelOrd' Empty = [[Nothing]]
levelOrd' (Node x Empty Empty) = [[Just x]]
levelOrd' (Node x l r) = [Just x] : zipWith' (levelOrd' l) (levelOrd' r)

symmetricTree :: (Eq a) => Btree a -> Bool
symmetricTree  = and . map isSymmetric . tail . levelOrd'

inserts ele []  = [ele]
inserts ele (x:xs)
  | ele > x = x: inserts ele xs
  | ele == x = x:xs
  | otherwise = ele : x : xs


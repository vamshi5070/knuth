import Data.List

data Btree a = Empty
              | Node a (Btree a) (Btree a)
              deriving (Eq,Show)

insertNode :: (Ord a) => Btree a -> a -> Btree a
insertNode Empty ele = Node ele Empty Empty
insertNode (Node x l r) ele
  | x < ele = Node x (insertNode l ele) r
  | x > ele = Node x l (insertNode r ele)
  | otherwise = Node x l r

deleteNode :: (Ord a) => Btree a -> a -> Btree a
deleteNode Empty _ = Empty
deleteNode (Node x l r) ele
  | x == ele = subTreeInsL r l
  | x < ele = Node x l (deleteNode r ele)
  | otherwise = Node x  (deleteNode l ele) r

sub = Node 2 (Node 1 Empty Empty) (Node 3 Empty Empty)

full = Node 8 (Node 6 (Node 5 Empty Empty) (Node 7 Empty Empty)) (Node 10 Empty Empty)

myTree =  Node 5 (Node 3 (Node 2 Empty Empty) (Node 4 Empty Empty)) (Node 8 (Node 6 Empty Empty) (Node 10 Empty Empty))

subTreeInsL :: (Ord a) => Btree a -> Btree a -> Btree a
subTreeInsL Empty y = y
subTreeInsL (Node x l r) y = Node x (subTreeInsL l y) r

subTreeInsR :: (Ord a) => Btree a -> Btree a -> Btree a
subTreeInsR Empty y = y
subTreeInsR (Node x l r) y = Node x l (subTreeInsR r y)

elemTree :: (Ord a) => Btree a -> a -> Bool
elemTree Empty _ = False
elemTree (Node x l r) ele = (x == ele) || elemTree l ele || elemTree r ele

instance Functor Btree where
  fmap _ Empty = Empty
  fmap f (Node v l r) = Node (f v) (fmap f l) (fmap f r)

instance Foldable Btree where
  --  foldMap :: Monoid m => (a -> m) -> t a -> m
  foldMap _ Empty = mempty
  foldMap f (Node v l r) = f v <> foldMap f l <> foldMap f r

-- validBST :: (Ord a) => (Btree a) -> Bool
validBST Empty = True
validBST (Node x l r) = all' (x>) l && all' (x<=) r && validBST l && validBST r
  where all' pred tree = and $ pred <$> tree

paths :: Btree a -> [[a]]
paths Empty = [[]]
paths (Node x Empty Empty) = [[x]]
paths (Node x l r) = (x:) <$> (paths l ++ paths r)

maskm xs ys = xs ++ drop (length xs) ys

leftView tree = foldl maskm [] $ paths tree

rightView tree = foldl maskm [] $ reverse $ paths tree

rightView' tree = foldr (flip maskm) [] $ paths tree

inOrder Empty = []
inOrder (Node x l r) = inOrder l ++ [x] ++ inOrder r

preOrder Empty = []
preOrder (Node x l r) = [x] ++ preOrder l ++ preOrder r

-- make tree with inorder and preorder
makeTree :: (Eq a) => [a] -> [a] -> Btree a
makeTree [] [] = Empty
makeTree [] _ = error "empty preorder"
makeTree _ []  = error "empty inorder"
makeTree (p:pre) inO = Node p (makeTree lpre linO) (makeTree rpre rinO)
  where lpre = pre `intersect` linO
        rpre = pre `intersect ` rinO
        linO = takeWhile (/=p) inO
        rinO = tail $ dropWhile (/=p) inO

----------------------------------------------------------

zip' :: [[a]] -> [[a]] -> [[a]]
zip' l [] = l
zip' [] r = r
zip' (x:xs) (y:ys) = (x++y): zip' xs ys

levelOrder :: Btree a -> [[a]]
levelOrder Empty = []
levelOrder (Node a l r) = [a] : zip' (levelOrder l) (levelOrder r)

--levelOrder :: Btree a -> [[a]]
--levelOrder Empty = []

lv' t = head <$> levelOrder t
rv' t = last <$> levelOrder t

main = return ()

tree = "susheela"

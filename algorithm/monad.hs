maybePlus :: Maybe Int -> Maybe Int -> Maybe Int
maybePlus x y = do
  resX <- x
  resY <- y
  return $ resX + resY

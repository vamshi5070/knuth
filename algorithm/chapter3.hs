main :: IO ()
main = return ()

type SymList a = ([a],[a])

fromSL :: SymList a -> [a] -- from symlist to normal list
fromSL (xs,ys) = xs ++ reverse ys

lastSL :: SymList a -> a
lastSL (xs,ys)
  | null ys && null xs = error "empty list"
  | null ys = head xs
  | otherwise = head ys

snocSL :: a -> SymList a -> SymList a
snocSL ele (xs,ys)
  | null xs = (ys,[ele])
  | otherwise = (xs,ele:ys)

nilSL :: SymList a
nilSL = ([],[])

tailSL :: SymList a -> SymList a
tailSL (xs,ys)
  | null xs && null ys = error "empty list"
  | null xs = nilSL
  | single xs = (vs,us)
  | otherwise = (tail xs,ys)
  where (us,vs) = splitAt (length ys `div` 2) ys

single :: [a] -> Bool
single [x] = True
single _  = False

consSL :: a -> SymList a -> SymList a
consSL x (xs,ys)
  | null ys = ([x],xs)
  | otherwise = (x:xs,ys)

initSL :: SymList a -> SymList a
initSL (xs,ys)
  | null xs && null ys = error "empty list"
  | null xs  = (vs,us)
  | otherwise = (xs,tail ys)
  where (us,vs) = splitAt (length ys `div` 2) ys

nullSL :: SymList a -> Bool
nullSL (xs,ys)  = null xs && null ys

-- singleSL :: SymList a -> Bool
-- singleSL ()

lengthSL :: SymList a -> Int
lengthSL (xs,ys) = length xs + length ys

singleSL :: SymList a -> Bool
singleSL sym = lengthSL sym == 1

headSL :: SymList a -> a
headSL xss@(xs,ys)
  | nullSL xss = error "empty symlist"
  | null xs = head ys
  | otherwise = head xs

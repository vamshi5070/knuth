import Data.Bifunctor

merge :: (Ord a) => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys  = ys
merge xs ys = foldr inserts ys xs

inserts :: (Ord a) => a -> [a] -> [a]
inserts ele [] = [ele]
inserts ele (x:xs) 
  | ele <= x = ele:x:xs
  | otherwise = x: inserts ele xs

insertionSort :: Ord a => [a] -> [a]
insertionSort = foldr inserts []

splitAt2 = splittake . lengthFinder
  where splittake (xs,n) = splitAt n xs
        lengthFinder xs = (xs,length xs `div` 2)

mergeSort :: Ord a => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = uncurry merge . (bimap mergeSort mergeSort) . splitAt2 $ xs
  --do
  -- (ys,zs) <- splitAt2 xs
  -- return (mergeSort ys,mergeSort zs)
  
  -- where
  --   merger (us,vs)  = merge us vs
  --   helper xs = splitAt2 xs
  --   (us,vs) = splitAt2 xs
  --   helper =
main = return ()

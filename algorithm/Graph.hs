{-# LANGUAGE TupleSections#-}

-- import Control.Monad
import Data.List
import Data.Maybe

newtype UnEdge a = UnEdge (a,a)
                deriving (Show)

instance Eq a => Eq (UnEdge a) where
  UnEdge (x,y) == UnEdge (x1,y1) = (x == x1) && (y == y1) || (y == x1) && (x == y1)

newtype Graph a = Graph [UnEdge a]
                  deriving (Eq,Show)

myGraph = Graph [UnEdge ('a','b'), UnEdge ('b','c') ,UnEdge ('a','c'), UnEdge ('x','a'),UnEdge ('b','z'),UnEdge ('z','c'),UnEdge ('a','w'),UnEdge ('c','w')]

vertices :: (Eq a) => Graph a -> [a]
vertices (Graph xs) = nub result
  where
    result = do
      UnEdge (x,y) <- xs
      [x,y]

adj :: (Eq a) => Graph a -> a -> [a]
adj (Graph xs) node = nub $ xs >>= helper
  where helper (UnEdge (x,y))
          | x == node = [y]
          | y == node = [x]
          | otherwise = []

isAdj :: Eq a => Graph a -> a -> a -> Bool
-- is_adj graph start end =  elem end (adj graph start)
isAdj (Graph xs) start end = UnEdge (start,end) `elem` xs

isWalk :: Eq a => Graph a -> [a] -> Bool
isWalk _ [] = True
isWalk _ [_] = True
isWalk graph (x:y:xs) = isAdj graph x y && isWalk graph (y:xs)

connectNearest :: [a] -> [UnEdge a]
connectNearest [] = []
connectNearest [x] = []
connectNearest (x:y:xs) = UnEdge (x,y) : connectNearest (y:xs)

isTrail :: Eq a => Graph a -> [a] -> Bool
isTrail graph xs = isWalk graph xs && (length res == (length . nub $ res))
  where res = connectNearest xs

isPath :: Eq a => Graph a -> [a] -> Bool
isPath graph xs = isWalk graph xs && (length xs == (length . nub $ xs))

open :: (Eq a) => Graph a -> [a] -> Bool
open _ [] = True
open _ [x] = True
open graph ys = (head ys /= last ys) &&  null ( ys \\ vertices graph)

close :: (Eq a) => Graph a -> [a] -> Bool
close graph xs = not  $ open graph xs

paths  :: (Eq a) => Graph a -> a -> a -> [[a]]
paths graph start end = go [] graph start end
  where go stack graph start end
          | start == end = [[start]]
          | otherwise =  (end:) <$> ((adj graph end \\ stack) >>= go (end:stack) graph start)

step :: (Eq a) => Graph a ->   ([a],[a]) ->  ([a],[a])
step _ (visited,[]) = (visited,[])
step graph (visited,node:queue) = (visited ++[node], ((adj graph node \\ visited )\\ queue) <> queue )

dfs g a = fst $ fromJust $ find (null . snd) $ iterate (step g) ([],[a])

bfs :: (Eq a) => Graph a -> [a] -> [a] -> [a]
bfs _ _ [] = []
bfs graph visited (node:queue) = node:bfs graph visited' queue'
  where visited' = node:visited
        queue' =  queue <> ((adj graph node \\ visited )\\ queue)

-- bfs level order
bfsL :: (Eq a) => Graph a -> [(a,Int)] -> [(a,Int)] -> [(a,Int)]
bfsL _ _ [] = []
bfsL graph visited ((node,level):queue) = (node,level):bfsL graph visited' queue'
  where visited' = (node,level):visited
        queue' =  queue <> (( ,level+1) <$> refined)
        refined = (adj graph node \\ map fst  visited ) \\ map fst queue

bfsl graph start
  | start `elem` graph_vertices = bfsL graph [] [(start,0)]
  | otherwise = bfsL graph [] []
  where graph_vertices = vertices graph

shortestPaths  :: Eq a => Graph a -> a -> a -> [[a]]
shortestPaths graph start end = filter (\x -> length x == minLen) res
  where res = paths graph start end
        minLen = minimum . map length $ res

test xs = do -- same as map
  x <- xs
  return (+2) x


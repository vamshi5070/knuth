import Data.List
import qualified Data.Map as M

newtype UnEdge a w = UnEdge (a,a,w)
                deriving (Show)

instance (Eq a ,Eq w) => Eq (UnEdge a w) where
  UnEdge (x,y,weight) == UnEdge (x1,y1,weight1) = (weight == weight1 ) && (x == x1) && (y == y1) || (y == x1) && (x == y1)

newtype Graph a w = Graph [UnEdge a w]
  deriving (Eq,Show)

vertices :: (Eq a) => Graph a w -> [a]
vertices (Graph xs) = nub $ xs >>= (\(UnEdge (x,y,_)) -> [x,y] )

myGraph :: Graph Char Int
myGraph = Graph [UnEdge ('a','b',1), UnEdge ('b','c',2) , UnEdge ('x','a',2),UnEdge ('b','z',3),UnEdge ('z','c',1),UnEdge ('a','w',1),UnEdge ('c','w',1)]

adj :: (Eq a) => Graph a w -> a -> [(a,w)]
adj (Graph xs) node = xs  >>= helper --xs >>= (\x -> helper x )
  where helper (UnEdge (x,y,weight))
          | x == node = [(y,weight)]
          | y == node = [(x,weight)]
          | otherwise = []

labell ::(Eq a) => Graph a w -> a -> [(a,Maybe w)]
labell graph start = [(node,Nothing) | (node,_) <- res]
  where res = adj graph start

starter :: (Eq a) => Graph a w -> [(a,Maybe w)]
starter graph = (\x -> (x,Nothing)) <$> vertices graph

data Dist a = Inf
  | Dist a
  deriving (Eq,Show)

instance Functor Dist where
  fmap _ Inf = Inf
  fmap f (Dist a) = Dist $ f a

instance Applicative Dist where
  pure = Dist
  (<*>) Inf _ = Inf
  (<*>) _ Inf  = Inf
  (<*>) (Dist f) (Dist a) = Dist $ f a

instance Monad Dist where
  return = pure
  (>>=) Inf _ = Inf
  (>>=) (Dist a) f = f a

instance (Ord a) => Ord (Dist a) where
  compare Inf Inf = EQ
  compare Inf _ = GT
  compare _ Inf = LT
  compare (Dist a) (Dist b) = compare a b

class DPQ q where
  mdel :: (Ord v, Ord k) => q k v -> q k v
  emin :: (Foldable (q k) , Ord v) => q k v -> (k,v)
  upd :: (Ord v,Ord k) => q k v -> q k v ->  q k v

-- instance DPQ M.Map where

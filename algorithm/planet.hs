import Data.Char (ord)

main = return ()


data Planet = Mercury
            | Venus
            | Earth
            | Mars
            | Jupiter
            | Saturn
            | Uranus
            | Neptune
ageOn :: Planet -> Float -> Float
ageOn Mercury sec  = sec * 0.2408467/31557600
ageOn Venus sec  = sec * 0.61519726/31557600
ageOn Earth sec  = sec * 1/31557600
ageOn Mars sec  = sec * 1.8808158/31557600
ageOn Jupiter sec  = sec * 11.862615/31557600
ageOn Saturn sec  = sec * 29.447498/31557600
ageOn Uranus sec  = sec * 84.016846/31557600
ageOn Neptune sec  = sec * 164.79132/31557600


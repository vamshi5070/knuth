import Control.Monad

insertIfNotEmpty :: a -> [[a]] -> [[[a]]]
insertIfNotEmpty _ [] = []
insertIfNotEmpty ele (x:xs) = ((ele:x):xs):((x:) <$> (insertIfNotEmpty ele xs))

-- partition :: [a] -> [[[a]]]
-- partition [] = [[]]
-- partition (x:xs) = (\p -> ([x]:p)):

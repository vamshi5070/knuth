import System.IO
import Control.Monad.Loops
import Data.Set(fromList)
import Data.List.Split(chunksOf)
-- import Data.List
import Control.Applicative
import Data.Maybe

takeEvery3 :: [a] -> [a]
takeEvery3 (_:_:x:xs) = x:(takeEvery3 xs)
takeEvery3 _ = []

takeEveryn n xs
  | length xs >= n = head xs' : takeEveryn n (tail xs')
  | otherwise = []
    where xs' = drop (n-1) xs

group n xs = take n xs : group n (drop n xs)

l [] func = func 0
l (_:xs) func = l xs (func . succ)

repN :: (Monad m) => Int ->  m a ->  m ()
repN 0 _ = return ()
repN n a = a >> repN (n-1) a

for [] fa  = return ()
for (x:xs) fa = fa x >> for xs fa

sequence_' :: (Foldable t, Monad m) => t (m a) -> m ()
sequence_'  = foldr (>>) (return ())

rep n h = print n
          >> getLine
          >>= (\l -> (hPutStrLn h ( (show n) ++ "::" ++  l))
          >> (if (l == "exit")
                then hClose h >> (print "end!")
                else (rep (n+1) h)))

islike n = print (show n ++ "?") >> getLine >>= (\str -> return $ if str == "rgv" then True else False)

data Btree a = Leaf a
             | Node a (Btree a) (Btree a)
             deriving (Eq,Show)

mytree = Node 3 (Leaf 4) (Leaf 5)
mytree1 = Node 110 (Leaf 4) (Leaf 115)
mytree2 = Node 33 (Leaf 14) (Leaf 5)
mytree12 = Node 10 (Node 1 (Leaf 13) (Leaf 0)) (Leaf 15)
alltree = Node 3 mytree1 mytree12

all_paths :: Btree a -> [[a]]
all_paths (Leaf a) = [[a]]
all_paths (Node a l r) = (a:) <$> (all_paths l) ++ (all_paths r)

unique :: (Eq a) => a -> [a] -> [a]
unique ele [] = [ele]
unique ele (x:xs)
  | ele == x = x:xs
  | otherwise = ele:x:xs

(!!!) :: Int -> [a] -> Maybe a
(!!!) i xs
  | (i >= 0) && (length xs > i) = Just $ xs !! i
  | otherwise = Nothing

makeOne :: (Eq a) => [a] -> [a]
makeOne xs = go [] xs
  where go acc [] = acc
        go acc (x:xs)
          | not (elem x acc) = go (x:acc) xs
          | otherwise = go acc xs

height :: Btree a -> Int
height = maximum . map length . all_paths

-- iter :: (Ord a, Num [a]) => [a] -> [Int] -> [[Maybe a]]
-- iter :: Int -> [[Int]] -> [[Maybe Int]]
-- iter n xs
-- | n >= 0 = (map (\x -> x !!! n) xs) :( iter (n-1) xs)
-- | otherwise = [[]]
cutter tree = ((!!!) <$> [0..(height tree)-1])

level tree = fmap fromList $ chunksOf (length $ all_paths tree) $ (cutter tree)  <*> (all_paths tree)

zipWith' f xs [] = xs
zipWith' f [] ys = ys
zipWith' f xs ys = zipWith f xs ys

levels :: Btree a -> [[a]]
levels (Leaf x) = [[x]]
levels (Node x l r) = [[x]] <> zipWith' (<>) (levels l) (levels r)

-- data

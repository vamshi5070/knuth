import Data.List
import Control.Monad

-- perms :: [Int] -> Int -> [[Int]]
-- perms xs n = (take n xs):[]

-- perm xs = do
  -- x <- xs
  -- y <- delete x xs
  -- return [x,y]

perm xs =  xs >>= (\x -> delete x xs >>= (\y -> return [x,y]))

inserts :: Int -> [Int] -> [[Int]]
inserts num = map (:[num])
--(\x -> [num,x]) 

perm2 _ 0 = [[]]
perm2 xs n = xs >>= (\x -> (x:) <$> perm2 (delete x xs) (n-1))

p xs = [1..length xs] >>=  perm2 xs 

comb xs = do
  let nxs = zip [1..] xs
  (n,x) <- nxs
  y <- [s | (m,s) <- nxs , n < m]
  return [x,y]

-- for sorted lists
combination _ 0 = [[]]
combination xs n = do
  x <- xs
  (x:) <$> combination [y |  y <- xs , y > x] (n-1)

-- for unsorted lists
-- combination _ 0 = [[]]
-- combination xs n = do
  -- let nxs = zip [1..] xs
  -- (nx,x) <- nxs
  -- combination xs n = (nxs >>= (\(nx,x) -> (x:) <$> (combination [y | (m,y) <- nxs , n < m] (n-1)))) where nxs = zip [1..] xs

-- powerSet xs = go xs (length xs)
  -- where go _ 0 = [[]]
        -- go xs n = (combination xs n) <> go xs (n-1)

powerSet xs =   [0..length xs] >>= combination xs

cartProduct xs ys = do
  x <- xs
  y <- ys
  return [x,y]

xmonad :: [Char]
xmonad = "rgv"

-- cartProduct' xs ys = [[x,y] | x <- xs, y <- ys ]

cartProductn _ 0 = [[]]
cartProductn xs n = do
  x <- xs
  (x:) <$> cartProductn xs (n-1)

-- powerSet []  = [[]]
-- powerSet :: [a] ->  [[a]]
-- powerSet  = foldr (\x (acc:accs) -> (x:acc):acc:accs  ) [[]]

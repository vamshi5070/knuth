main :: IO ()
main = return ()


is7 n
  | n == 1 = "one"
is7 n
  |  n == 7 = "seven"

divides k n = rem k n == 1

ldf k n
  | k `divides` n = k
  | k^2 > n = n
  | otherwise = ldf (n+1) k

removeFst :: (Eq a) => [a] -> a -> [a]
removeFst [] _ = []
removeFst (x:xs) ele
  | x == ele = xs
  | otherwise = x:removeFst xs ele

min' :: (Ord a) => a -> a -> a
min' x y
  | x <= y = x
  | otherwise = y

minimum' :: (Ord a) => [a] -> a
minimum' [] = error "empty list"
minimum' (x:xs) = min' x (minimum' xs)

selectionSort :: (Ord a) => [a] -> [a]
selectionSort []  = []
selectionSort xs = x : selectionSort (removeFst xs x)
  where x = minimum' xs

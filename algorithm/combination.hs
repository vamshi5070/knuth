-- for unsorted lists
combination _ 0 = [[]]
-- combination xs n = do
--   let nxs = zip [1..] xs
--   (nx,x) <- nxs
combination xs n = (nxs >>= (\(nx,x) -> (x:) <$> (combination [y | (m,y) <- nxs , n < m] (n-1)))) where nxs = zip [1..] xs

perm :: [a] -> [[a]]
perm [] = [[]]
perm (x:xs) =  (perm xs) >>= (inserts x) 
-- perm (x:xs) = concatMap (inserts x)  (perm xs)
  -- x <- xs
  -- ys <- inserts  x xs
  -- perm ys
  -- inserts x xs >>= perm

inserts :: a -> [a] -> [[a]]
inserts x [] = [[x]]
inserts x (y:xs) =  (x:y:xs) : ( map (y:) $ inserts x xs)

main = do
  putStr "Enter the shift num: "
  num <- getInt
  putStr "Enter the message: "
  str <- getLine
  print $ num
  putStrLn $ "After encryption: " <> encryption num str
  putStrLn $ "After decryption: " <> decryption num (encryption num str)

data Cipher = Encrypt | Decrypt deriving (Eq, Show)

getInt :: IO Int
getInt =  fmap read getLine

shiftChar :: Cipher -> Int -> Char -> Char
shiftChar Encrypt n ch = (iterate mySucc ch) !! n
shiftChar Decrypt n ch = (iterate myPred ch) !! n

myPred :: Char -> Char
myPred 'a' = 'z'
myPred ch  = pred ch

mySucc :: Char -> Char
mySucc 'z' = 'a'
mySucc ch  = succ ch

encryption :: Int -> String -> String
encryption n str = map (shiftChar Encrypt n) str

decryption :: Int -> String -> String
decryption n str = map (shiftChar Decrypt n) str

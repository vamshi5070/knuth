minmax :: Ord a => [a] -> (a,a)
minmax [x] = (x,x)
minmax [x,y]
  | x <= y = (x,y)
  | otherwise = (y,x)
minmax xs =  (min x1 x2, max y1 y2) 
  where (x1,y1) = minmax ys
        (x2,y2) = minmax zs
        (ys,zs) = halve xs
        
halve :: [a] -> ([a],[a])
halve  = splitAt 2

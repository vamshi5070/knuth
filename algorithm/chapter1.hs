main :: IO ()
main = return ()

-- length' :: [a] -> Int
-- length'  = foldl myFunc 0
--   where myFunc n _ = succ n

-- myFunc :: (Enum a,Enum b) => a -> b -> a
-- myFunc  x = succ   (flip const ) x

head' :: [a] -> a
head' = foldr const exception
  where exception = error "empty list"

foldr' ::  (a -> b -> b) -> b -> [a] -> b
foldr' f acc [] = acc
foldr' f acc (x:xs) = f x (foldr' f acc xs)

takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' p xs = foldr (\x acc -> if p x then x : acc else []) [] xs

foldl' ::  (b -> a -> b) -> b -> [a] -> b
foldl' f acc [] =  acc
foldl' f acc (x:xs) = foldl f acc' xs
  where acc' =  f acc x

-- scanr'  :: (a -> b -> b) -> b -> [a] -> [b]
-- scanr' f acc [] = [acc]
-- scanr' f acc (x:xs) = f x acc : scanr' f acc xs

inserts :: a -> [a] -> [[a]]
inserts x [] = [[x]]
inserts x (y:ys) = (x:y:ys) : ( (y:) <$> (inserts x ys))

perm1 :: [a] -> [[a]]
perm1 [] = [[]]
perm1 (x:xs) = do
  y <- perm1 xs
  inserts x y

picks :: [a] -> [(a,[a])]
picks [] = []
picks (x:xs) = (x,xs): map (\(y,ys) -> (y,x:ys)) (picks xs)

perm2 :: [a] -> [[a]]
perm2 [] = [[]]
perm2 xs = do
  (y,ys) <- picks xs
  (y:) <$> perm2 ys

append :: [a] -> [a] -> [a]
append  = flip (foldr (:))

foldr'' :: (a -> b -> b) -> b -> [a] -> b
foldr'' f acc xs
  | null xs = acc
  | otherwise = f (head xs) (foldr'' f acc (tail xs))

foldl'' ::  (b -> a -> b) -> b -> [a] -> b
foldl'' f acc xs
  | null xs = acc
  | otherwise = foldl f acc' (tail xs)
  where acc' = f acc (head xs)

-- steep :: [Int] -> Bool
-- steep xs =  [((sum xs) - x < x) | x <- xs]
-- steep [] = True
-- steep (x:xs) = (x > sum xs) && steep xs
steep = snd . faststeep

faststeep [] = (0,True)
faststeep (x:xs) = (x+s,x>s && b) where (s,b) = faststeep xs

steep' xs  = snd $ foldr (\x (s,b)  -> (x+s,x > s && b)) (0,True) xs

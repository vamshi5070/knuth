import Data.Char
import Data.List 
import Control.Monad
import Data.Map (Map,fromList,insertWith,empty)

inserts :: Char -> [Char] -> [Char]
inserts ' ' str = str
inserts ch "" = [ch]
inserts ch (x:xs)
  | ch == x = x:xs
  | ch < x = ch:x:xs
  | otherwise = x: inserts ch xs

check :: String -> Bool
check str = nub (sort str') == ['a'..'z']++['Ã']
  where str' = filter isAlpha str

allCapital xs = [] == filter isLower xs

responseFor :: String -> String
responseFor xs
  | last xs == '?' = "Sure."
  | allCapital xs = "Whoa, chill out!"
  | allCapital xs && last xs == '?' = "Whoa, chill out!"
  | res == xs = "Fine. Be that way!"
  | otherwise = "Whatever."
  where res = filter  (\x -> elem x "\t\n \r"    ) xs
  
conv 'G' =  Right 'C'
conv 'C' = Right 'G'
conv 'T' = Right 'A'
conv 'A' = Right 'U'
conv  x = Left x   

-- conv 'G'  =  Right 'C'
-- conv 'C' = Right 'G'
-- conv 'T' = Right 'A'
-- conv 'A' = Right 'U'
-- conv  x = Left x   

data Nucleotide = A | C | G | T
  deriving (Eq, Ord, Show)

custInsert
  :: Num a =>
     Char -> Map Nucleotide a -> Either Char (Map Nucleotide a)
custInsert 'G' map = Right $ insertWith (+) G 1 map
custInsert 'A' map = Right $ insertWith (+) A 1 map
custInsert 'C' map = Right $ insertWith (+) C 1 map
custInsert 'T' map = Right $ insertWith (+) T 1 map
custInsert x _ = Left x

  
myM xs = foldr myFunc (Right myMap ) xs
  where myMap = fromList [(A,0)
                         ,(C,0)
                         ,(G,0)
                         ,(T,0)
                         ]

myFunc ::
 Num a =>
  Char -> Either Char (Map Nucleotide a) ->  Either Char (Map Nucleotide a) 
myFunc ch (Left ch1) = Left ch1
myFunc ch (Right map) =  custInsert ch map


-- helper :: Char -> [(Char,Int)] -> [(Char,Int)] 
-- helper 'A' [('A',n)]

sumOfMultiples factors limit = res1 - res2
  where res1 = sum $ map (\x -> helper x limit) factors
        res2 = product factors

helper x limit = x * (k*(k+1)`div` 2) 
        where k = kFinder x
              kFinder x = (limit - 1) `div` x

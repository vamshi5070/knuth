-- import Data.List

difference  :: Eq a => [a] -> [a] -> [a]
difference xs ys =  filter ( not . (flip   elem ys) ) xs

-- partition :: (Eq a) =>
-- (a -> Bool) -> [a] -> ([a], [a])
-- partition p xs = (first,second)
  -- where first = filter p xs
        -- second = xs `difference` first

partition :: (a -> Bool) -> [a] -> ([a], [a])
partition p xs = (filter p xs , filter (not . p) xs)

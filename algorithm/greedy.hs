import Data.List
  
pairs :: [a] -> [(a,a)]
pairs [] = error "empty list"
pairs [x] = error "single element list"
pairs xs = twoListPair iXs tXs ++ [(x,y)]
  where iXs = init xs
        tXs = tail xs
        x = head xs
        y = last xs

twoListPair :: [a] -> [a] -> [(a,a)]
twoListPair []  [] = []
twoListPair (x:xs) (y:ys) = (x,y) : twoListPair xs ys

pairs' :: [a] -> [(a, a)]
pairs' xs = [(x,y) | x : ys <- tails xs, y : zs <- tails ys]

pairsM :: [a] -> [(a,a)]
pairsM xs = do
  x:ys <- tails xs
  y:zs <- tails ys
  [(x,y)]

picks :: [a] -> [(a,[a])]
picks [] = []
picks (x:xs) = do
  (y,ys) <- picks xs
  [(y,x:ys)]

picks' :: [a] -> [(a,[a])]
picks' [] = []
picks' (x:xs) = (x,xs): (map (\(y,ys) -> (y,x:ys)) $ picks' xs)

selectionSort :: (Ord a) => [a] -> [a]
selectionSort [] = []
selectionSort xs = y:selectionSort ys
  where (y,ys) = minimum $ picks' xs

inserts :: (Ord a) => a -> [a] -> [[a]]
inserts ele [] = [[ele]]
inserts ele (x:xs) = (ele:x:xs) : map (x:) (inserts ele xs)

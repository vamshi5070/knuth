import Data.List
import Data.Array

type Nat = Int

inc' :: Int -> Int
inc' num = num + 1

length' :: [a] -> Nat
length' [] = 0
length' (_:xs) = inc' (length' xs)

length'' :: [a] -> Nat
length'' = foldr succ' 0
  where succ' _ n = n+1

head' :: [a] -> a
head' = foldr const (error "empty list")
  -- where x << _ = x

inserts :: a -> [a] -> [[a]]
inserts x [] = [[x]]
inserts x (y:ys) = (x:y:ys): map (y:) (inserts x ys)

perms :: [a] -> [[a]]
perms = foldr helper [[]] 
  where helper x xs = concatMap (inserts x) xs

picks [] = []
picks (x:xs) = (x,xs) : map (\(y,ys) -> (y,x:ys)) zs
  where zs = picks xs
  -- do
  -- (y,ys) <- picks xs
  -- [(x,xs),(y,x:xs)]


perms2 :: [a] -> [[a]]
perms2 [] = [[]]
perms2  xs = concatMap subperms ( picks xs)
  where subperms (y,ys) =  map (y:) (perms2 ys)

maximum' :: (Ord a) => [a] -> a
maximum' [] = error "empty list" 
maximum' [x]= x
maximum' (x:xs) = max x (maximum' xs)

minimum' :: (Ord a) => [a] -> a
minimum' [] = error "empty list" 
minimum' [x]= x
minimum' (x:xs) = min x (minimum' xs)

take'  :: Int -> [a] -> [a]
take' 0 _ = []
take' _ [] = []
take' n (x:xs) = x : take' (n-1) xs

drop' :: Int -> [a] -> [a]
drop' 0 xs = xs
drop' _ [] = []
drop' n (x:xs) = drop' (n-1) xs

takeWhile' :: (a -> Bool) -> [a] -> [a]
takeWhile' _ [] = []
takeWhile' p (x:xs)
  | p x = x : takeWhile' p xs
  | otherwise = []

takeWhile'' :: (a -> Bool) -> [a] -> [a]
takeWhile'' p  = foldr (\x acc -> if p x then x:acc else []) [] 

dropWhileEnd'  :: (a -> Bool) -> [a] -> [a]
dropWhileEnd' p xs = foldr (\x acc -> if  p x && null xs then [] else x:acc) [] xs

dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' _ [] = []
dropWhile' p (x:xs)
  | p x =  dropWhile' p xs
  | otherwise = x:xs

inits' :: [a] -> [[a]]
inits'   = helper 0
  where helper n xs
          | n <= length xs = take n xs:helper (n+1) xs
          | otherwise = []

tails' :: [a] -> [[a]]
tails'   = helper 0
  where helper n xs
          | n <= length xs = drop n xs:helper (n+1) xs
          | otherwise = []

splitAt' :: Int -> [a] -> ([a], [a])
splitAt' n xs = (take n xs,drop n xs)

span' :: (a -> Bool) -> [a] -> ([a], [a])
span' p xs = (takeWhile' p xs,dropWhile' p xs)

null' :: [a] -> Bool
null' [] = True
null' _ = False

elem' :: (Eq a) => a -> [a] -> Bool
elem' _   [] = False
elem' ele (x:xs)    
  | ele == x = True
  | otherwise = elem' ele xs

access :: [a] -> Int -> a  -- similar to (!!)
access [] _ = error "index out of range"
access (x:_) 0 = x
access (x:xs) n 
  | n > 0 = access xs (n-1)
  | otherwise = error "index out of range"

all' :: (a -> Bool)  -> [a] -> Bool
all' _ [] = True
all' p (x:xs) = p x && all' p xs

any' :: (a -> Bool)  -> [a] -> Bool
any' _ [] = False
any' p (x:xs) = p x || any' p xs

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys 

uncons' :: [a] -> Maybe (a, [a])
uncons' [] = Nothing
uncons' (x:xs) = Just ( x , xs)


wrap :: a -> [a]
wrap = pure

unwrap :: [a] -> a
unwrap xs
  | single xs = head xs
  | otherwise = error "not singleton list"

single ::[a] -> Bool
single [x] = True
single _ = False

reverse' :: [a] -> [a]
reverse'  = helper [] 
  where helper acc [] = acc
        helper acc (x:xs) = helper (x:acc ) xs

reverse''  :: [a] -> [a]
reverse'' xs = foldl (flip (:)) [] xs

map' :: (a -> b) -> [a] -> [b]
map' f xs = foldr (\x acc -> f x : acc) [] xs

filter' ::  (a -> Bool) -> [a] -> [a]
filter' p  = foldr (\x acc -> if p x then x:acc else acc) []

-- integer :: [Int] -> Integer
integer xs = fst $ foldr (\x (acc,exp) -> (x * 10^exp + acc,exp+1)) (0,0) xs

fraction xs = res/10^zeros
  where res = fst $ foldr (\x (acc,exp) -> (x * 10^exp + acc,exp+1)) (0,0) xs
        zeros = snd $ foldr (\x (acc,exp) -> (x * 10^exp + acc,exp+1)) (0,0) xs

rgv xs = snd $ foldr (\x (acc,exp) -> (x * 10^exp + acc,exp+1)) (0,0) xs

iterate'' f x = f x:iterate'' f (f x)

apply :: Nat -> (a -> a) -> a -> a
apply n f x = last $ take n $ iterate'' f x

rotString :: Int -> String -> [String]
rotString _ "" = error "empty string"
rotString 0 _ = []
rotString n (ch:str) = newStr: rotString (n-1) newStr 
  where newStr = str++[ch]

helper :: String -> [String]
helper str = rotString (length str) str

remove :: (Eq a) => a -> [a] -> [a]
remove _ [] = [] 
remove x (y:ys) 
  | x == y = remove x ys
  | otherwise = y: remove x ys

--main = interact $ concat . tail . words 

folds :: (Eq a) => [a] -> [a]
folds [] = []
folds (x:xs) = x:folds (remove x xs)

mkarray xs = listArray (0,length xs -1) xs


data Tree a = Null
            | Node a (Tree a) (Tree a)
            deriving (Eq,Show)

heapify :: Ord a => Tree a -> Tree a
heapify Null = Null
heapify (Node x l r) = siftDown x (heapify l) (heapify r)

siftDown :: Ord a => a -> Tree a -> Tree a -> Tree a
siftDown ele Null Null = Node ele Null Null
siftDown ele (Node x l r) Null
  | ele <= x = Node ele (Node x l r) Null
  | otherwise = Node x (siftDown ele l r) Null
siftDown ele Null (Node x l r)
  | ele <= x = Node ele Null (Node x l r)
  | otherwise = Node x Null (siftDown ele l r)
siftDown ele (Node x l r) (Node y l1 r1)
  | ele <= min x y = Node ele  (Node x l r) (Node y l1 r1)
  | x <= min ele y = Node x (siftDown ele l r) (Node y l1 r1) --(siftDown ele l r)v
  | y <= min ele x = Node y (Node x l r) (siftDown ele l1 r1)

tree1 = Node 2 Null (Node 5 Null Null)
tree2 = Node 5 (Node 33 Null Null) (Node 52 Null Null)
treeRGV = Node 44 tree1 tree2

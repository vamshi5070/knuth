------------------------------------------------------------------------------------------------------------
data BinaryTree a =
  Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

instance Functor BinaryTree where
  fmap _ Leaf = Leaf
  fmap f (Node l x r) = Node  (fmap f l) (f x) (fmap f r)

testTree :: BinaryTree Integer
testTree =
     Node (Node  (Node Leaf 6 Leaf) 1 (Node Leaf 6 Leaf) )
          2
          (Node   (Node Leaf 10 Leaf) 3 (Node Leaf 0 Leaf))

myTree :: Ord a => [a] -> BinaryTree a
myTree  = foldr  insertBST  Leaf

insertLeft :: Ord a => a -> (a -> a -> Ordering) ->  BinaryTree a -> BinaryTree a
insertLeft element _ Leaf = Node Leaf element Leaf
insertLeft element comp (Node l x r)
  | comp element  x == GT = Node (insertLeft x comp l) element r
  | comp element  x == LT = Node (insertLeft element comp l) x r
  | otherwise = Node l x r

insertAtLeft :: Ord a => a -> (a -> a -> Ordering) -> BinaryTree a -> BinaryTree a
insertAtLeft element comp (Node l x r)
  | comp element x == LT = Node (insertHeap element comp l) x r
  | comp element x == GT = Node (insertHeap x comp l) element r
  | otherwise = Node l x r

insertAtRight :: Ord a => a -> (a -> a -> Ordering) -> BinaryTree a -> BinaryTree a
insertAtRight element comp (Node l x r)
  | comp element  x == LT = Node l x (insertHeap element comp r)
  | comp element x == GT = Node l element (insertHeap  x comp r)
  | otherwise = Node l x r

isComplete :: BinaryTree a ->  Bool
isComplete tree = helperComplete tree (count tree) 0
  where helperComplete Leaf _ _ = True
        helperComplete (Node l _ r) n i
          | n > i = helperComplete l n (2*i+1) && helperComplete r n (2*i+2)
          | otherwise = False

insertHeap :: Ord a => a -> (a -> a -> Ordering) -> BinaryTree a -> BinaryTree a
insertHeap element _ Leaf = Node Leaf element Leaf
insertHeap element comp (Node Leaf x Leaf) = insertAtLeft element comp (Node Leaf x Leaf)
insertHeap _ _ (Node Leaf _ _) = error "Invalid heap"
insertHeap element comp (Node l x Leaf) = insertAtRight element comp (Node l x Leaf)
insertHeap element comp (Node l x r)
  | lEqR ( Node l x r) = insertLeft element comp (Node l x r)
  | not (lEqR l)  = insertAtLeft element comp (Node l x r)
  | otherwise  =  insertAtRight  element comp (Node l x r)

deleteRight :: BinaryTree a -> (a , BinaryTree a)
deleteRight Leaf = error "Invalid Heap"
deleteRight (Node Leaf x Leaf) = (x ,Leaf)
deleteRight (Node  l x r) = (val,Node l x tree)
  where (val,tree) = deleteRight r

subtree :: BinaryTree Int
subtree = Node (Node (Node Leaf 4 Leaf) 8 (Node Leaf 3 Leaf)) 10 (Node (Node Leaf 2 Leaf) 7 (Node Leaf 1 Leaf))

popHeap :: Ord a => (a -> a -> Ordering) -> BinaryTree a -> (a,BinaryTree a)
popHeap comp tree = (x,adjustHeap comp (Node l val r))
  where (val,Node l x r) = deleteLast tree

adjustHeap :: Ord a => (a -> a -> Ordering) -> BinaryTree a -> BinaryTree a
adjustHeap _ Leaf = error "Invalid Heap"
adjustHeap _ (Node Leaf x Leaf) = Node Leaf x Leaf
adjustHeap _ (Node Leaf _ _) = error "Invalid Heap"
adjustHeap comp (Node (Node l a r) x Leaf)
  | comp x  a == GT = Node (Node l a r) x Leaf
  | comp x  a == LT = Node (adjustHeap comp (Node l x r)) a Leaf

adjustHeap comp (Node (Node l a r) x (Node l1 a1 r1))
  | (comp a a1 == GT && comp a1 x == GT) || (comp a x == GT && comp x a1 == GT) = Node (adjustHeap comp (Node l x r)) a (Node l1 a1 r1)
  | (comp a1 a == GT && comp a x == GT) || (comp a1 x == GT && comp x a == GT)= Node (Node l a r) a1 (adjustHeap comp (Node l1 x r1))
  | otherwise = Node (Node l a r) x (Node l1 a1 r1)

tobeAdjusted :: BinaryTree Int
tobeAdjusted = Node (
  Node (
      Node (
          Node Leaf 2 Leaf) 6 Leaf) 8 (Node Leaf 5 Leaf)) 1 (Node (Node Leaf 4 Leaf) 7 (Node Leaf 3 Leaf))

myList :: Integral a => [a]
myList = [100,34,677,-34,676,34545,65434,5,553]

heapSort :: Ord a => (a -> a -> Ordering) ->  [a] -> [a]
heapSort comp = treeToList comp  . createTree comp

createTree :: Ord a => (a -> a -> Ordering) -> [a] -> BinaryTree a
createTree comp  = foldr (`insertHeap` comp ) Leaf

treeToList :: Ord a => (a -> a -> Ordering) -> BinaryTree a -> [a]
treeToList _ Leaf = []
treeToList _ (Node Leaf x Leaf) = [x]
treeToList comp tree = val : treeToList comp mytree
  where (val,mytree) = popHeap comp tree

deleteLast :: BinaryTree a -> (a , BinaryTree a)
deleteLast Leaf = error "Invalid Heap"
deleteLast (Node Leaf x Leaf) = (x,Leaf)
deleteLast (Node Leaf _ _) = error "Invalid heap"
deleteLast (Node l x Leaf) = (val,Node tree x Leaf)
  where (val,tree) = deleteLast l
deleteLast (Node l x r) = (val,Node ltree x rtree)
  where ltree
          | lEqR (Node l x r)  = l
          | not (lEqR r) = l
          | otherwise = snd $ deleteLast l
        val
          | lEqR (Node l x r) = fst $ deleteRight r
          | not (lEqR r) = fst $ deleteLast r
          | otherwise = fst $ deleteLast l
        rtree
          | lEqR (Node l x r) = snd $ deleteRight r
          | not (lEqR r) = snd $ deleteLast r
          | otherwise = r

-- valid :: BinaryTree a -> Bool
-- valid tree = and [not (leftEmpty tree), full removedTree , lEqR removedTree ]
  -- where removedTree = removeLast tree

lEqR :: BinaryTree a -> Bool
-- lEqR Leaf = False
lEqR tree = 2 ^ heightLeft tree - 1 == count tree

count :: BinaryTree a -> Int
count Leaf = 0
count (Node l _ r) = 1 + count l + count r

leftEmpty :: BinaryTree a -> Bool   -- checks if only left is empty
leftEmpty Leaf = False
leftEmpty (Node Leaf _ Leaf) = False
leftEmpty (Node Leaf _ _) = True
leftEmpty (Node l _ r) = leftEmpty l || leftEmpty r

testHeap = Node (Node (Node Leaf 1 Leaf) 3 Leaf) 4 (Node Leaf 2 Leaf)

heightLeft :: BinaryTree a -> Int
heightLeft Leaf = 0
heightLeft (Node l _ _) = 1 + heightLeft l

removeLast :: BinaryTree a -> BinaryTree a
removeLast Leaf = Leaf
removeLast (Node l x r) = Node (removeLast l) x (removeLast r)

full :: BinaryTree a -> Bool
full Leaf = True
full (Node Leaf _ Leaf) = True
full (Node Leaf _ _) = False
full (Node _ _ Leaf) = False
full (Node l _ r) = full l && full r
------------------------------------------------------------------------------------------------------------
insertBST :: (Ord a) => a -> BinaryTree a -> BinaryTree a
insertBST x Leaf = Node Leaf x Leaf
insertBST x (Node left a right)
    | x ==a = Node left a right
    | x < a = checkAndRotate $ Node (insertBST x left) a right
    | x > a = checkAndRotate $ Node left a (insertBST x right)

elemBST :: (Ord a) => a -> BinaryTree a -> Bool
elemBST _ Leaf = False
elemBST x (Node l a r)
  | x == a = True
  | x < a = elemBST x l
  | otherwise = elemBST x r

mapTree :: (a -> b) -> BinaryTree a -> BinaryTree b
mapTree _ Leaf = Leaf
mapTree f (Node left a right) =
     Node (mapTree f left) (f a) (mapTree f right)

preorder :: BinaryTree a -> [a]
preorder Leaf = []
preorder (Node left x right) = [x] ++ preorder left ++ preorder right

inorder :: BinaryTree a -> [a]
inorder Leaf = []
inorder (Node left x right) = inorder left ++ [x] ++ inorder right

postorder :: BinaryTree a -> [a]
postorder Leaf = []
postorder (Node left x right) = postorder left  ++ postorder right  ++ [x]

main :: IO ()
main = do
         let myTree = foldr insertBST Leaf [1..10]
         let myTree1 = mapTree (+3) myTree
         print $ inorder myTree1

bstree :: Num a => BinaryTree a
bstree = Node (Node (Node Leaf 10 Leaf) 20 (Node Leaf 30 Leaf)) 40 (Node (Node Leaf 45 Leaf) 50 (Node Leaf 60 Leaf))

heightBST :: BinaryTree a -> Int
heightBST Leaf = 0
heightBST (Node l _ r) = 1 + max (heightBST l) (heightBST r)

balFactor :: BinaryTree a -> Int
balFactor Leaf = 0
balFactor (Node l _ r) = heightBST l - heightBST r

llRot :: BinaryTree a -> BinaryTree a
llRot (Node (Node bl b br) a c) = Node bl b (Node br a c)

lrRot :: BinaryTree a -> BinaryTree a
lrRot (Node (Node bl b (Node cl c cr)) a ar) = Node (Node bl b cl) c (Node cr a ar)

rrRot :: BinaryTree a -> BinaryTree a
rrRot (Node al a (Node bl b c)) = Node (Node al a bl) b c

rlRot :: BinaryTree a -> BinaryTree a
rlRot (Node al a (Node (Node cl c cr) b br)) = Node (Node al a cl) c (Node cr b br)

checkAndRotate :: BinaryTree a -> BinaryTree a
-- checkAndRotate (Node Leaf x r) =
checkAndRotate (Node l x r)
  | balFactor (Node l x r) == 0 = Node l x r
  | (balFactor (Node l x r) == 2) && (balFactor l == -1) = lrRot (Node l x r)
  | (balFactor (Node l x r) == 2) && (balFactor l == 1) = llRot (Node l x r)
  | (balFactor (Node l x r)== -2) && (balFactor l == 1) = rlRot (Node l x r)
  | (balFactor (Node l x r) == -2) && (balFactor l == -1) = rrRot (Node l x r)
-- lrRot (Node (Node Leaf x (Node Leaf x1 Leaf) ) a Leaf) = llRot $ Node  (Node (Node Leaf x Leaf) x1 Leaf ) a Leaf
-- rlRot (Node Leaf a (Node (Node Leaf x1 Leaf) x Leaf)) = rrRot $ Node Leaf a (Node Leaf x1 (Node Leaf x Leaf))

returnLeft Leaf = Leaf
returnLeft (Node l _ _) = l

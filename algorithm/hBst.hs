type Nat = Int

-- main = return ()

data Btree a = Null
              | Node Nat a (Btree a) (Btree a)
              deriving (Eq,Show)

height :: Btree a -> Nat
height Null = 0
height (Node h _ _ _) = h

createNode :: a -> Btree a  -> Btree a -> Btree a
createNode x l r = Node h x l r
  where h = 1 + max (height l) (height r)

insertNode :: (Ord a) => Btree a -> a -> Btree a
insertNode Null ele = Node 1 ele Null Null
insertNode (Node h x l r) ele
  | x > ele = gbalance  x (insertNode l ele) 
  | x < ele = gbalance  x l (insertNode r ele)
  | otherwise = Node h x l r

bias :: Btree a -> Int
bias Null = 0
bias (Node _ x l r) = height l - height r

rotr :: Btree a -> Btree a
rotr (Node _ x (Node _ y ll lr) r) = createNode y ll (createNode x lr r)

rotl :: Btree a -> Btree a
rotl (Node _ x l (Node _ y rl rr)) = createNode y (createNode x l rl) rr

test :: Btree Char
test =  createNode 'a' (createNode 'b' (createNode 'c' Null Null)  (createNode 'e' Null Null)) (createNode 'd' Null Null)

balance :: a -> Btree a -> Btree a -> Btree a
balance x l r
  | abs diff <= 1 = createNode x l r
  | diff == 2 = rotateR x l r
  | diff == -2 = rotateL x l r
  where h1 = height l
        h2 = height r
        diff = h1 - h2

rotateR :: a -> Btree a -> Btree a -> Btree a
rotateR x l r
  | bias l >= 0 = rotr (createNode x l r)
  | otherwise = rotr (createNode x (rotl l) r)

rotateL :: a -> Btree a -> Btree a -> Btree a
rotateL x l r
  | bias l <= 0 = rotl (createNode x l r)
  | otherwise = rotl (createNode x l (rotr r) )

balanceR :: a -> Btree a -> Btree a -> Btree a
balanceR x (Node _ y ll lr) r
  | diff >=  2 = balanceR y ll lr
  | otherwise =  balance y ll (createNode x lr r)
  where h1 = height lr
        h2 = height r
        diff = h1 - h2

balanceL :: a -> Btree a -> Btree a -> Btree a
balanceL x l (Node _ y rl rr)
  | diff <=  -2 = balanceL y rl rr
  | otherwise =  balance y (createNode x l rr) rl
  where h1 = height rl
        h2 = height l
        diff = h1 - h2

gbalance :: a -> Btree a -> Btree a -> Btree a
gbalance x l r
  | abs (h1 - h2) <= 2 = balance x l r
  | h1 - h2 > 2 = balanceR x l r
  | h1 - h2 < 2 = balanceL x l r
  where h1 = height l
        h2 = height r

member :: (Ord a) => a -> Btree a -> Bool
member _ Null = False
member ele (Node _ x l r)
  | x == ele = True
  | x > ele = member ele l
  | otherwise = member ele r

delete :: Ord a => a -> Btree a -> Btree a
delete _ Null = Null
delete ele (Node _ x l r)
  | x > ele = balance x (delete x l) r
  | x < ele = balance x l (delete x r)
  | otherwise = combine l r

deleteMin :: Ord a => Btree a -> (a,Btree a)
deleteMin Null = error "empty list"
deleteMin (Node _ x Null r) = (x,r)
deleteMin (Node _ x l r) =  (ele,balance x res r)
  where (ele,res) = deleteMin l

deleteMax :: Ord a => Btree a -> (a,Btree a)
deleteMax Null = error "empty list"
deleteMax (Node _ x  l Null) = (x,l)
deleteMax (Node _ x l r) =  (ele,balance x l res)
  where (ele,res) = deleteMax r

combine :: Ord a => Btree a -> Btree a -> Btree a
combine l Null = l
combine Null r = r
combine l r = balance x l res
  where (x,res) = deleteMin r

data Piece a = LP (Btree a) a
               | RP a (Btree a)
               deriving (Eq,Show)

pieces :: (Ord a) => a -> Btree a ->  [Piece a]
pieces ele t = addPiece t [] where
  addPiece Null acc = acc
  addPiece (Node _ x l r) acc
    | ele < x = addPiece l (RP x r:acc)
    | otherwise = addPiece r (LP l x:acc)

sew :: (Ord a) => [Piece a] -> (Btree a,Btree a)
sew = foldl helperSew (Null,Null)
  where helperSew (t1,t2) (LP t x)  = (gbalance x t1 t,t2 )
        helperSew (t1,t2) (RP x t)  = (t1,gbalance x t t2 )

splitTree :: (Ord a) => a -> Btree a -> (Btree a,Btree a)
splitTree ele  = sew . pieces ele
  

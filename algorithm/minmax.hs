minmax :: (Ord a) => [a] -> (a,a)
minmax [x] = (x,x)
minmax [x,y]
  | x <= y = (x,y)
  | otherwise = (y,x)
minmax xs = (min x1 x2, max y1 y2) 
  where (x1,y1) = minmax ys
        (x2,y2) = minmax zs
        (ys,zs) = halve xs


halve :: [a] -> ([a],[a])
halve xs  = splitAt (length xs `div` 2) xs 

until' :: (a -> Bool) -> (a -> a) -> a -> a
until' p f x
  | p x =  x
  | otherwise = until' p f (f x) 

partition3 :: (Ord a) => a -> [a] -> ([a],[a],[a])
partition3 ele = foldr op ([],[],[])
  where op x (xs,ys,zs) 
          | x < ele = (x:xs,ys,zs)
          | x == ele = (xs,x:ys,zs)
          | otherwise = (xs,ys,x:zs)

select :: Ord a => Int -> [a] -> a
select _ []  = error "empty list"
select k xs
  | k <= m = select k us
  | k <= m+n = vs !!(k -m-1)
  | k > m+n = select (k -m-n) ws
  where (us,vs,ws) = partition3 (pivot xs) xs
        (m,n) = (length us,length vs)

pivot :: [a] -> a
pivot = head




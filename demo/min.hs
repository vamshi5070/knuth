min' x y
  | x <= y = x
  | otherwise = y


minimum' [] = error "empty list"
minimum' [x] = x
minimum' (x:xs) = min' x $ minimum' xs


main  = do
  putStr "Min is "
  print $ minimum' [1..5]

testDefined :: Int -> String -> a
testDefined = undefined


import Test.Hspec
import Test.QuickCheck

main = hspec $ do
  describe "Addition" $ do
    it "1 + 1 is greater than 2" $ do
      (1+ 1) > 1 `shouldBe` True
  it " x + 1 is always greater than x" $ do
    property $ \x -> x + 1 > (x :: Int)
      

trivialInt :: Gen Int
trivialInt = return 1

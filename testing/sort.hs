import Test.QuickCheck
import Data.List

propSort :: ([Int] -> [Int]) -> [Int] -> Bool
propSort f xs = f xs == sort xs

runQcMerge :: IO ()
runQcMerge = quickCheck (propSort mergeSort)

runQcInsert :: IO ()
runQcInsert = quickCheck (propSort insertionSort)

inserts :: (Ord a) => a -> [a] -> [a]
inserts ele [] = [ele]
inserts ele (x:xs) 
  | ele <= x = ele:x:xs
  | otherwise = x:inserts ele xs

merge :: (Ord a) => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys  = ys
merge xs ys = foldr inserts ys xs

insertionSort :: (Ord a) => [a] -> [a]
insertionSort [] = []
insertionSort (x:xs) = inserts x (insertionSort xs)

splitAt2 :: [a] -> ([a],[a])
splitAt2 xs = splitAt mid xs
  where mid = length xs `div` 2

mergeSort :: (Ord a) => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = merge us vs
  where (ys,zs) = splitAt2 xs
        us = mergeSort ys
        vs = mergeSort zs

oneThroughThree :: Gen Int
oneThroughThree = elements [1,2,3]


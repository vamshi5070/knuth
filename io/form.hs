import Control.Monad
import Data.Foldable (for_)

main = do
  forM [[1,4],[2,23]] (print . sum)
    
maximum' []  = error "empty list"
maximum' [x] = x
maximum' (x:xs) = x `max'` (maximum' xs)
                          
max' :: Ord a => a -> a -> a
max' a b
  | a >= b = a
  | otherwise = b

maybeFunc1 :: String -> Maybe Int
maybeFunc1 "" = Nothing
maybeFunc1 str = Just . length $ str

maybeFunc2 :: Int -> Maybe Float
maybeFunc2 n
  | even n = Nothing
  | otherwise = Just ((fromIntegral n) * 3.14519)

maybeFunc3 :: Float -> Maybe [Int]
maybeFunc3 flt
  | flt > 15.0 = Nothing
  | otherwise = Just [floor flt, ceiling flt]

runMaybeFuncs :: String -> Maybe [Int]
runMaybeFuncs input = do
  n <- maybeFunc1 input
  flt <- maybeFunc2 n
  maybeFunc3 flt
